/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    loadDvds();

    $('#add-button').click(function (event) {
        event.preventDefault();

        $.ajax({
            type: 'POST',
            url: 'dvd',
            data: JSON.stringify({
                title: $('#add-title').val(),
                releaseDate: $('#add-release-date').val(),
                rating: $('#add-rating').val(),
                directorName: $('#add-director-name').val(),
                studio: $('#add-studio').val(),
                userNotes: $('#add-user-notes').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) { //after adding, replace this form with empty strings
            $('#add-title').val('');
            $('#add-release-date').val('');
            $('#add-rating').val('');
            $('#add-director-name').val('');
            $('#add-studio').val('');
            $('#add-user-notes').val('');
            loadDvds();
        });
    });

    $('#edit-button').click(function (event) {
        event.preventDefault();

        $.ajax({
            type: 'PUT',
            url: 'dvd/' + $('#edit-identifier').text(), 
            data: JSON.stringify({
                title: $('#edit-title').val(),
                releaseDate: $('#edit-release-date').val(),
                rating: $('#edit-rating').val(),
                directorName: $('edit-director-name').val(),
                studio: $('#edit-studio').val(),
                userNotes: $('#edit-user-notes').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            loadDvds();
        });
    });


});


function loadDvds() {
    clearDvdTable();


    var cTable = $('#contentRows');

    $.ajax({
        type: 'GET',
        url: 'dvds'
    }).success(function (data, status) {

        $.each(data, function (index, dvd) {
            cTable.append($('<tr>')
                    .append($('<td>').append($('<a>')
                            .attr({
                                'data-identifier': dvd.identifier,
                                'data-toggle': 'modal',
                                'data-target': '#detailsModal'
                            })
                            .text(dvd.title + ' ' + dvd.releaseDate)))

                    .append($('<td>').text(dvd.releaseDate))
                    .append($('<td>').append($('<a>')
                            .attr({
                                'data-identifier': dvd.identifier,
                                'data-toggle': 'modal',
                                'data-target': '#editModal'
                            }).text('Edit')))
                    .append($('<td>')
                            .append($('<a>').attr({
                                'onclick': 'deleteDvd(' + dvd.identifier + ')'
                            }).text('Delete')))

                    );
        });
    });
}

function clearDvdTable() {
    $("#contentRows").empty();
}
//on show.bs.modal is an event handler
$('#detailsModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);
    var identifier = element.data('identifier');
    var modal = $(this); //event called by modal dialog, stored in this var

    $.ajax({
        url: 'dvd/' + identifier
    }).success(function (dvd) {
        
        modal.find('#details-identifier').text(dvd.identifier);
        modal.find('#dvd-title').text(dvd.title);
        modal.find('#dvd-releaseDate').text(dvd.releaseDate);
        modal.find('#dvd-rating').text(dvd.rating);
        modal.find('#dvd-directorName').text(dvd.directorName);
        modal.find('#dvd-studio').text(dvd.studio);
        modal.find('#dvd-userNotes').text(dvd.userNotes);
    });
});

$('#editModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);
    var identifier = element.data('identifier');

    var modal = $(this);

    $.ajax({
        url: 'dvd/' + identifier
    }).success(function (dvd) {

        modal.find('#edit-identifier').text(dvd.identifier);

        modal.find('#edit-title').val(dvd.title);
        modal.find('#edit-release-date').val(dvd.releaseDate);
        modal.find('#edit-rating').val(dvd.rating);
        modal.find('#edit-director-name').val(dvd.directorName);
        modal.find('#edit-studio').val(dvd.studio);
        modal.find('#edit-user-notes').val(dvd.userNotes);

    });

});

function deleteDvd(identifier) {
    var answer = confirm("Do you really want to delete this DVD entry?");
    if (answer === true) {
        $.ajax({
            type: 'DELETE',
            url: 'dvd/' + identifier
        }).success(function () {
            loadDvds();
        });
    }
}