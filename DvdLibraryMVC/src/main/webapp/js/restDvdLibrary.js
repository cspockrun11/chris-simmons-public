/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    loadDvds();
    
    $('#add-button').click(function (event) {
        event.preventDefault();
        
        $.ajax({
            type: 'POST',
            url: 'dvd',
            data: JSON.stringify({
                title: $('#add-title').val(),
                releaseDate: $('#add-release-date').val(),
                rating: $('#add-rating').val(),
                directorName: $('#add-director-name').val(),
                studio: $('#add-studio').val(),
                userNotes: $('#add-user-notes').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            $('#add-title').val('');
            $('#add-release-date').val('');
            $('#add-rating').val('');
            $('#add-director-name').val('');
            $('#add-studio').val('');
            $('#add-user-notes').val('');
            loadDvds();
        });
    });
    
    $('#edit-button').click(function (event) {
        event.preventDefault();
        
        $.ajax({
            type: 'PUT', //because edit
            url: 'dvd/' + $('#edit-dvd-id').val(), // CHECK FOR OTHER IDENTIFIER IDS
            data: JSON.stringify({
                title: $('#edit-title').val(),
                releaseDate: $('#edit-release-date').val(),
                rating: $('#edit-rating').val(),
                directorName: $('#edit-director-name').val(),
                studio: $('#edit-studio').val(),
                userNotes: $('#edit-user-notes').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).success(function (data,status) {
            hideEditForm();
            loadDvds();
        });
    });
    
});

function loadDvds() {
    clearDvdTable();
    
    var contentRows = $('#contentRows');
    
    $.ajax({
        type: 'GET',
        url: 'dvds'
    }).success(function (data, status) {
        $.each(data, function (index, dvd) {
            var title = dvd.title;
            var date = dvd.releaseDate;
            var id = dvd.identifier;
            
            var row = '<tr>';
            row += '<td>' + title + '</td>';
            row += '<td>' + date + '</td>';
            row += '<td><a onclick="showEditForm(' + id + ')">Edit</a></td>';
            row += '<td><a onclick="deleteDvd(' + id + ')">Delete</a></td>';
            row += '</tr>';
            contentRows.append(row);
        });
    });
}

function clearDvdTable() {
    $('#contentRows').empty();
}


function deleteDvd(identifier) {
    $.ajax({
        type: 'DELETE',
        url: 'dvd/' + identifier
    }).success(function () {
        hideEditForm();
        loadDvds();
    });
}

function showEditForm(identifier) {
    
    $.ajax({
        type: 'GET',
        url: 'dvd/' + identifier
    }).success(function (dvd, status) {
        $('#edit-title').val(dvd.title);
        $('#edit-release-date').val(dvd.releaseDate);
        $('#edit-rating').val(dvd.rating);
        $('#edit-director-name').val(dvd.directorName);
        $('#edit-studio').val(dvd.studio);
        $('#edit-user-notes').val(dvd.userNotes);
        $('#edit-dvd-id').val(dvd.identifier);
        
        $('#editFormDiv').show();
        $('#addFormDiv').hide();
    });
}

function hideEditForm() {
    $('#edit-title').val('');
        $('#edit-release-date').val('');
        $('#edit-rating').val('');
        $('#edit-director-name').val('');
        $('#edit-studio').val('');
        $('#edit-user-notes').val('');
        $('#edit-dvd-id').val('');
        
        $('#addFormDiv').show();
        $('#editFormDiv').hide();
}
