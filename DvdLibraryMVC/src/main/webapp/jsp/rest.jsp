<!DOCTYPE html>
<html>
    <head>
        <title>Dvd Library w Rest</title>
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css"
              rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1>Dvd Library Web Application (REST)</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" ><a href="${pageContext.request.contextPath}/home">Home</a></li>
                    <li role="presentation" ><a href="${pageContext.request.contextPath}/search">Search</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/stats">Stats</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/displayDvdLibraryNoAjax">Dvd Library (no Ajax)</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/rest">Dvd Library (using REST)</a></li>
                </ul>
            </div>
            <!--
            Add a row to our container - this will hold the summary table and the new contact form.
            -->
            <div class="row">
                <!-- #2: Add a col to hold the summary table - have it take up half the row -->
                <div class="col-md-6">
                    <div id="contactTableDiv">
                        <h2>My DVDs</h2>

                        <table id="dvdTable" class="table table-hover">
                            <tr>
                                <th width="40%">Dvd Title</th>
                                <th width="30%">Release Date</th>
                                <th width="15%"></th>
                                <th width="15%"></th>
                            </tr>
                            <!--
                             #3: This holds the list of contacts - we will add rows
                            dynamically
                             using jQuery
                            -->
                            <tbody id="contentRows"></tbody>
                        </table>
                    </div>
                </div> <!-- End col div -->
                <!--
                #4: Add col to hold the new contact form - have it take up the other half of the row
                -->
                <div class="col-md-6">

                    <div id="editFormDiv" style="display: none">
                        <h2 onclick="hideEditForm()">Edit Contact</h2>

                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="edit-title" class="col-md-4 control-label">
                                    Title:
                                </label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-title"
                                           placeholder="Title"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-release-date" class="col-md-4 control-label">
                                    Release Date:
                                </label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-release-date"
                                           placeholder="Release Date">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-rating" class="col-md-4 control-label">
                                    MPAA Rating:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-rating"
                                           placeholder="MPAA Rating"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-director-name" class="col-md-4 control-label">Director Name:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-director-name"
                                           placeholder="Director Name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-studio" class="col-md-4 control-label">Studio:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-studio"
                                           placeholder="Studio"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-user-notes" class="col-md-4 control-label">User Notes:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-user-notes"
                                           placeholder="Add a description...."/>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-4">
                                    <input type="hidden" id="edit-dvd-id">
                                    <button type="button"
                                            id="edit-cancel-button"
                                            class="btn btn-default"
                                            onclick="hideEditForm()">
                                        Cancel
                                    </button>
                                </div>
                                <div class="col-md-4">
                                    <button type="submit"
                                            id="edit-button"
                                            class="btn btn-default">
                                        Update Dvd
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div id="addFormDiv">

                        <h2>Add New Dvd</h2>

                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="add-title" class="col-md-4 control-label">
                                    Title:
                                </label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-title"
                                           placeholder="Title"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-release-date" class="col-md-4 control-label">
                                    Release Date:
                                </label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-release-date"
                                           placeholder="Release Date"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-rating" class="col-md-4 control-label">
                                    MPAA Rating:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-rating"
                                           placeholder="MPAA Rating"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-director-name" class="col-md-4 control-label">Director Name:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-director-name"
                                           placeholder="Director Name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-studio" class="col-md-4 control-label">Studio:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-studio"
                                           placeholder="Studio"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-user-notes" class="col-md-4 control-label">User Notes:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-user-notes"
                                           placeholder="Add a description...."/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8">
                                    <button type="submit"
                                            id="add-button"
                                            class="btn btn-default">
                                        Create New Dvd
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div> <!-- End col div -->
            </div> <!-- End row div -->
        </div>
        <!-- #5: Placed at the end of the document so the pages load faster -->
        <script src="js/jquery-1.11.3.min.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/restDvdLibrary.js"></script>
    </body>
</html>