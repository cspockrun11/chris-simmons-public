<%-- 
    Document   : editDvdFormNoAjax
    Created on : Mar 29, 2016, 3:31:40 PM
    Author     : apprentice
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/img/icon.png" rel="shortcut icon">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Dvd Library</title>
    </head>
    <body><div class="container">
            <h1>Dvd Library</h1>
            <hr/>
        </div>

        <div class="container">
            <h2>Edit Dvd Form</h2>
            <a href="displayDvdLibraryNoAjax">Dvd Library (no Ajax)</a><br>
            <hr/>

            <sf:form class="form-horizontal" role="form" action="editDvdNoAjax" modelAttribute="dvd" method="POST">
                <div class="form-group">
                    <label for="title" class="col-md-4 control-label">Title:</label>
                    <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="edit-title" path="title" placeholder="Title" />
                                            <sf:errors path="title" cssClass="text text-danger"></sf:errors>

                    </div>
                </div>
                <div class="form-group">
                    <label for="release-date" class="col-md-4 control-label">Release Date:</label>
                    <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="add-release-date" path="releaseDate" placeholder="Release Date" />
                                            <sf:errors path="releaseDate" cssClass="text text-danger"></sf:errors>

                    </div>
                </div>
                <div class="form-group">
                    <label for="rating" class="col-md-4 control-label">MPAA Rating:</label>
                    <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="add-rating" path="rating" placeholder="MPAA Rating" />
                                           <sf:errors path="rating" cssClass="text text-danger"></sf:errors>

                    </div>
                </div>
                <div class="form-group">
                    <label for="director-name" class="col-md-4 control-label">Director Name:</label>
                    <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="add-director-name" path="directorName" placeholder="Director Name" />
                                            <sf:errors path="directorName" cssClass="text text-danger"></sf:errors>

                    </div>
                </div>
                <div class="form-group">
                    <label for="studio" class="col-md-4 control-label">Studio:</label>
                    <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="add-studio" path="studio" placeholder="Studio" />
                                            <sf:errors path="studio" cssClass="text text-danger"></sf:errors>

                    </div>
                </div>
                <div class="form-group">
                    <label for="user-notes" class="col-md-4 control-label">User Comments:</label>
                    <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="add-user-notes" path="userNotes" placeholder="Add a description..." />
                                             <sf:errors path="userNotes" cssClass="text text-danger"></sf:errors>

                        <sf:hidden path="identifier"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" id="add-button" class="btn btn-default">Add New Dvd</button>
                    </div>
                </div>

            </sf:form>




        </div>



    </body>
</html>
