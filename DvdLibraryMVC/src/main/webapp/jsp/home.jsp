<%-- 
    Document   : home
    Created on : Mar 28, 2016, 6:47:02 PM
    Author     : apprentice
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DVD Library</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet"> 
        <link href="${pageContext.request.contextPath}/img/icon.png" rel="shortcut icon">

    </head>
    <body>
        <div class="container">
            <h1>DVD Library</h1>
            <hr />
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/home">Home</a></li>
                    <li role="presentation" ><a href="${pageContext.request.contextPath}/search">Search</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/stats">Stats</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/displayDvdLibraryNoAjax">Dvd Library (no Ajax)</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/rest">Dvd Library (using REST)</a></li>
                </ul>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <h2>My Dvds</h2>

                    <table id="dvdTable" class=" table table-hover">
                        <tr>
                            <th width="40%">Dvd Title</th>
                            <th width="30%">Release Date</th>
                            <th width="15%"></th>
                            <th width="15%"></th>
                        </tr>
                        <tbody id="contentRows"></tbody>
                    </table>
                </div>

                <!-- start of add form-->
                <div class="col-md-6">
                    <h2>Add New Dvd</h2>
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="add-title" class="col-md-4 control-label">Title</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-title" placeholder="Title" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-release-date" class="col-md-4 control-label">Release Date</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-release-date" placeholder="Release Date" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-rating" class="col-md-4 control-label">MPAA Rating</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-rating" placeholder="MPAA Rating" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-director-name" class="col-md-4 control-label">Director Name</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-director-name" placeholder="Directed By..." />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-studio" class="col-md-4 control-label">Studio</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-studio" placeholder="Studio" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-user-notes" class="col-md-4 control-label">User Notes</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-user-notes" placeholder="Add a description..." />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <button type="submit" id="add-button" class="btn btn-default">Create New Dvd</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </div>

        <div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="detailsModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="detailsModalLabel">Dvd Details</h4>
                    </div>
                    <div class="modal-body">
                        <h3 id="details-identifier"></h3>
                        <table class="table table-bordered">
                            <tr>
                                <th>Title</th>
                                <td id="dvd-title"></td>
                            </tr>
                            <tr>
                                <th>Release Date</th>
                                <td id="dvd-releaseDate"></td>
                            </tr>
                            <tr>
                                <th>Rating</th>
                                <td id="dvd-rating"></td>
                            </tr>
                            <tr>
                                <th>Director Name</th>
                                <td id="dvd-directorName"></td>
                            </tr>
                            <tr>
                                <th>Studio</th>
                                <td id="dvd-studio"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- start of edit modal -->

        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalDialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="editModalLabel">Edit Dvd</h4>
                    </div>
                    <div class="modal-body">
                        <h3 id="edit-identifier"></h3>
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="edit-title" class="col-md-4 control-label">Title</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-title" placeholder="Title" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-release-date" class="col-md-4 control-label">Release Date</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-release-date" placeholder="Release Date" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-rating" class="col-md-4 control-label">MPAA Rating</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-rating" placeholder="MPAA Rating" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-director-name" class="col-md-4 control-label">Director Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-title" placeholder="Director Name" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-studio" class="col-md-4 control-label">Studio</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-studio" placeholder="Studio" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-user-notes" class="col-md-4 control-label">User Notes</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-user-notes" placeholder="Add a description..." />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8">
                                    <button type="submit" id="edit-button" class="btn btn-default" data-dismiss="modal">Edit Dvd</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <input type="hidden" id="edit-dvd-id"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/mockData.js"></script>
        <script src="${pageContext.request.contextPath}/js/dvdLibrary.js"></script>
    </body>
</html>
