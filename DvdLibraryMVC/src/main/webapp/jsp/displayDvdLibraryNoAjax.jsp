<%-- 
    Document   : displayDvdLibraryNoAjax
    Created on : Mar 29, 2016, 3:14:37 PM
    Author     : apprentice
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DVD Library</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet"> 
        <link href="${pageContext.request.contextPath}/img/icon.png" rel="shortcut icon">
    </head>
    <body><div class="container">
            <h1>DVD Library</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/home">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/search">Search</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/stats">Stats</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/displayDvdLibraryNoAjax">Dvd Library (no Ajax)</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/rest">Dvd Library (using REST)</a></li>

                </ul>
            </div>
        </div>

        <div class="container">
            <h2>Dvd List</h2>
            <a href="displayNewDvdFormNoAjax">Add a Dvd</a><br>

            <c:forEach var="dvd" items="${dvdLibrary}">
                <s:url value="deleteDvdNoAjax" var="deleteDvd_url">
                    <s:param name="identifier" value="${dvd.identifier}" />
                </s:url>
                <s:url value="displayEditDvdFormNoAjax" var="editDvd_url">
                    <s:param name="identifier" value="${dvd.identifier}" />
                </s:url>

                Title: ${dvd.title} | Release Date: ${dvd.releaseDate} |
                <a href="${deleteDvd_url}">Delete</a> | <a href="${editDvd_url}">Edit</a> | <br>
                Rating: ${dvd.rating} | Directed By: ${dvd.directorName} | Studio: ${dvd.studio} <br>
                User Comments: ${dvd.userNotes} <br>
                <hr/>


            </c:forEach>




        </div>


        <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>
