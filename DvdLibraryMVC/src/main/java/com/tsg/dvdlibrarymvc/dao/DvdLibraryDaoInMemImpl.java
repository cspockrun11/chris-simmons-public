/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dvdlibrarymvc.dao;

import com.tsg.dvdlibrarymvc.dto.Dvd;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class DvdLibraryDaoInMemImpl implements DvdLibraryDao {

    private Map<Integer, Dvd> dvdMap = new HashMap<>();
    private int dvdIdCounter = 0;

    @Override
    public Dvd addDvd(Dvd dvd) {
        dvd.setIdentifier(dvdIdCounter);
        dvdIdCounter++;
        dvdMap.put(dvd.getIdentifier(), dvd);
        return dvd;

    }

    @Override
    public void removeDvd(int identifier) {
        dvdMap.remove(identifier);

    }

    @Override
    public void updateDvd(Dvd dvd) {
        dvdMap.put(dvd.getIdentifier(), dvd);

    }

    @Override
    public List<Dvd> getAllDvds() {
        Collection<Dvd> d = dvdMap.values();
        return new ArrayList(d);

    }

    @Override
    public Dvd getDvdById(int identifier) {
        return dvdMap.get(identifier);

    }

    @Override
    public List<Dvd> searchDvds(Map<SearchTerm, String> criteria) {

        String titleCriteria = criteria.get(SearchTerm.TITLE);
        String releaseDateCriteria = criteria.get(SearchTerm.RELEASE_DATE);
        String ratingCriteria = criteria.get(SearchTerm.RATING);
        String directorNameCriteria = criteria.get(SearchTerm.DIRECTOR_NAME);
        String studioCriteria = criteria.get(SearchTerm.STUDIO);
        String userNotesCriteria = criteria.get(SearchTerm.USER_NOTES);
        
        Predicate<Dvd> titleMatches;
        Predicate<Dvd> releaseDateMatches;
        Predicate<Dvd> ratingMatches;
        Predicate<Dvd> directorNameMatches;
        Predicate<Dvd> studioMatches;
        Predicate<Dvd> userNotesMatches;
        
        Predicate<Dvd> truePredicate = (d) -> {return true;};
        
        titleMatches = (titleCriteria == null || titleCriteria.isEmpty()) ? truePredicate : (d) -> d.getTitle().equals(titleCriteria);
        releaseDateMatches = (releaseDateCriteria == null || releaseDateCriteria.isEmpty()) ? truePredicate : (d) -> d.getReleaseDate().equals(releaseDateCriteria);
        ratingMatches = (ratingCriteria == null || ratingCriteria.isEmpty()) ? truePredicate : (d) -> d.getRating().equals(ratingCriteria);
        directorNameMatches = (directorNameCriteria == null || directorNameCriteria.isEmpty()) ? truePredicate : (d) -> d.getDirectorName().equals(directorNameCriteria);
        studioMatches = (studioCriteria == null || studioCriteria.isEmpty()) ? truePredicate : (d) -> d.getStudio().equals(studioCriteria);
        userNotesMatches = (userNotesCriteria == null || userNotesCriteria.isEmpty()) ? truePredicate : (d) -> d.getUserNotes().equals(userNotesCriteria);
        
        return dvdMap.values().stream()
                .filter(titleMatches
                        .and(releaseDateMatches)
                        .and(ratingMatches)
                        .and(directorNameMatches)
                        .and(studioMatches)
                        .and(userNotesMatches))
                .collect(Collectors.toList());
    }

}
