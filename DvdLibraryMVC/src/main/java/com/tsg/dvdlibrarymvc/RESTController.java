/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dvdlibrarymvc;

import com.tsg.dvdlibrarymvc.dao.DvdLibraryDao;
import com.tsg.dvdlibrarymvc.dto.Dvd;
import java.util.List;
import javax.inject.Inject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author apprentice
 */
@Controller
public class RESTController {
    
    private final DvdLibraryDao dao;
    @Inject
    public RESTController (DvdLibraryDao dao) {
        this.dao = dao;
    }
    
    @RequestMapping(value="/dvd/{id}", method=RequestMethod.GET)
    @ResponseBody
    public Dvd getDvd(@PathVariable("id") int identifier) {
        return dao.getDvdById(identifier);
    }
    
    @RequestMapping(value="/dvd", method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Dvd createDvd(@RequestBody Dvd dvd) {
        return dao.addDvd(dvd);
    }
    
    @RequestMapping(value="dvd/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteDvd(@PathVariable("id") int identifier) {
        dao.removeDvd(identifier);
    }
    
    @RequestMapping(value="dvd/{id}", method=RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateContact(@PathVariable("id") int identifier, @RequestBody Dvd dvd) {
        dvd.setIdentifier(identifier);
        dao.updateDvd(dvd);
    }
    
    @RequestMapping(value="/dvds", method=RequestMethod.GET)
    @ResponseBody
    public List<Dvd> getAllDvds() {
        return dao.getAllDvds();
    }
    
}
