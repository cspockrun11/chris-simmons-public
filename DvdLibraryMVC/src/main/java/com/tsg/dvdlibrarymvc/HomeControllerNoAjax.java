/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dvdlibrarymvc;

import com.tsg.dvdlibrarymvc.dao.DvdLibraryDao;
import com.tsg.dvdlibrarymvc.dto.Dvd;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class HomeControllerNoAjax {
    private DvdLibraryDao dao;
    
    @Inject
    public HomeControllerNoAjax(DvdLibraryDao dao) {
        this.dao = dao;
    }
    
    @RequestMapping(value="/displayDvdLibraryNoAjax", method=RequestMethod.GET)
    public String displayDvdLibraryNoAjax(Model model) {
        
        List<Dvd> dList = dao.getAllDvds();
        
        model.addAttribute("dvdLibrary", dList);
        
        return "displayDvdLibraryNoAjax";
    }
    
    @RequestMapping(value="displayNewDvdFormNoAjax", method=RequestMethod.GET)
    public String displayNewDvdFormNoAjax(Model model) {
        
        Dvd dvd = new Dvd();
        model.addAttribute("dvd", dvd);
           
        return "newDvdFormNoAjax";
    }
    
    @RequestMapping(value="/addNewDvdNoAjax", method=RequestMethod.POST)
    public String addNewDvdNoAjax (@Valid @ModelAttribute("dvd") Dvd dvd, BindingResult result) {
        
        if (result.hasErrors()) {
            return "newDvdFormNoAjax";
        }
        
//        String title = req.getParameter("title");
//        String releaseDate = req.getParameter("releaseDate");
//        String rating = req.getParameter("rating");
//        String directorName = req.getParameter("directorName");
//        String studio = req.getParameter("studio");
//        String userNotes = req.getParameter("userNotes");
//        
//        Dvd dvd = new Dvd();
//        dvd.setTitle(title);
//        dvd.setReleaseDate(releaseDate);
//        dvd.setRating(rating);
//        dvd.setDirectorName(directorName);
//        dvd.setStudio(studio);
//        dvd.setUserNotes(userNotes);
//        
        dao.addDvd(dvd);
        
        return "redirect:displayDvdLibraryNoAjax";
    }
    
    @RequestMapping(value="/deleteDvdNoAjax", method=RequestMethod.GET)
    public String deleteDvdNoAjax(HttpServletRequest req) {
        int identifier = Integer.parseInt(req.getParameter("identifier"));
        
        dao.removeDvd(identifier);
        
        return "redirect:displayDvdLibraryNoAjax";
    }
    
    @RequestMapping(value="/displayEditDvdFormNoAjax", method=RequestMethod.GET)
    public String displayEditDvdFormNoAjax(HttpServletRequest req, Model model) {
        int identifier = Integer.parseInt(req.getParameter("identifier"));
        
        Dvd dvd = dao.getDvdById(identifier);
        
        model.addAttribute("dvd", dvd);
        
        return "editDvdFormNoAjax";
    }
    
    @RequestMapping(value="/editDvdNoAjax", method=RequestMethod.POST)
    public String editDvdNoAjax(@Valid @ModelAttribute("dvd") Dvd dvd, BindingResult result) {
        
        if(result.hasErrors()) {
            return "editDvdFormNoAjax";
        }
        
        dao.updateDvd(dvd);
        
        return "redirect:displayDvdLibraryNoAjax";
        
    }
    
}
