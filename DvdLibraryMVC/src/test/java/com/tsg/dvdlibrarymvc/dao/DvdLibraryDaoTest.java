/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dvdlibrarymvc.dao;

import com.tsg.dvdlibrarymvc.dto.Dvd;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class DvdLibraryDaoTest {
    
    private DvdLibraryDao dao;
    private Dvd d1;
    private Dvd d2;
    private Dvd d3;
    
    public DvdLibraryDaoTest() {
    }
    
    @Before
    public void setUp() {
        
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("dvdLibraryDao", DvdLibraryDao.class);
        
        d1 = new Dvd();
        d1.setTitle("Star Wars");
        d1.setReleaseDate("1977");
        d1.setRating("G");
        d1.setDirectorName("Lucas");
        d1.setStudio("LucasFilm");
        d1.setUserNotes("not bad");
        
        d2 = new Dvd();
        d2.setTitle("Not Star Wars");
        d2.setReleaseDate("2001");
        d2.setRating("F");
        d2.setDirectorName("Lucas");
        d2.setStudio("LucasFilm");
        d2.setUserNotes("why");
        
        d3 = new Dvd();
        d3.setTitle("The Mighty Ducks");
        d3.setReleaseDate("1990");
        d3.setRating("G");
        d3.setDirectorName("Emilio Estevez");
        d3.setStudio("Something");
        d3.setUserNotes("DO YOU BELIEVE IN MIRACLES");
        
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void addGetDeleteDvds() {
        dao.addDvd(d1);
        
        Dvd fromDb = dao.getDvdById(d1.getIdentifier());
        
        assertEquals(fromDb, d1);
        
        dao.removeDvd(d1.getIdentifier());
        
        assertNull(dao.getDvdById(d1.getIdentifier()));
    }
    @Test
    public void addUpdateDvds() {
        dao.addDvd(d1);
        
        d1.setDirectorName("Williams");
        dao.updateDvd(d1);
        
        Dvd fromDb = dao.getDvdById(d1.getIdentifier());
        
        assertEquals(fromDb, d1);
               
    }
    
    @Test
    public void getAllDvdsTest() {
        dao.addDvd(d1);
        dao.addDvd(d2);
        dao.addDvd(d3);
        
        List<Dvd> dList = dao.getAllDvds();
        assertEquals(dList.size(), 3);
    }
    
    @Test
    public void searchDvdsTest() {
        dao.addDvd(d1);
        dao.addDvd(d2);
        dao.addDvd(d3);
        
        Map<SearchTerm, String> criteria = new HashMap<>();
        
        criteria.put(SearchTerm.TITLE, "Star Wars");
        List<Dvd> dlist = dao.searchDvds(criteria);
        assertEquals(1, dlist.size());
        assertEquals(d1, dlist.get(0));
        
        criteria.remove(SearchTerm.TITLE, "Star Wars");
        
        criteria.put(SearchTerm.STUDIO, "LucasFilm");
        List<Dvd> alist = dao.searchDvds(criteria);
        
        assertEquals(2, alist.size());
        assertEquals(d1, alist.get(0));
        
        
        criteria.put(SearchTerm.DIRECTOR_NAME, "Lucas");
        List<Dvd> rlist = dao.searchDvds(criteria);
        assertEquals(2, rlist.size());
        assertEquals(d1, rlist.get(0));
        assertEquals(d2, rlist.get(1));
     
        
    }
}
