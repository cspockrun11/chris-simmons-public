One of my very first Spring MVC Applications, modeled after coursework at The Software Guild. 
This app is a library system for a potentially very spacious DVD collection. Using Java, jdbcTemplates, Spring MVC, 
MySQL, phpMyAdmin, Bootstrap/CSS, JSPs, AJAX and RESTful endpoints, this application was built as a class but I configured
my version to see all of the working parts as a learning tool. 