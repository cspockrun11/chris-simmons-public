<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <!-- a jquery cdn -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.jpg">

    </head>
    <body>
        <div class="container">
            <h1><strong>Vending Machine</strong></h1>
            <h4>or, Let's Make A Deal</h4>
            <hr/>
        </div>
        <div class="container text-center">

            <form action="vending" method="POST">

                <input type="number" name="userMoney" id="userMoney" placeholder="How much money?" step="any" min="0" max="500"/>

                <br><br>
                <button class="btn btn-success" id="moneyButton" type="submit">Insert Money</button>

            </form>
                

        </div>



        <!-- Placed at the end of the document so the pages load faster -->
        <!--        <script src="{pageContext.request.contextPath}/js/jquery-1.11.0.min.js"></script>-->
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

