<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <!-- a goddamn jquery cdn -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
        <div class="container">
            <h1>Vending Machine</h1>
            <h4>or, Let's Make A Deal</h4>
            <hr/>

        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-9"> 

                    <div class="row text-center">

                        <div class="col-md-3">
                            <h3>A1</h3>
                            Snickers
                            <br>
                            Price: 0.75 
                        </div>
                        <div class="col-md-3">
                            <h3>A2</h3>
                            Twix   
                            <br>
                            Price: 0.75 
                        </div>
                        <div class="col-md-3">
                            <h3>A3</h3>
                            Skittles
                            <br>
                            Price: 1.00
                        </div>
                    </div>
                    <hr/>
                    <div class="row text-center">

                        <div class="col-md-3">
                            <h3>B1</h3>
                            Kit-Kat
                            <br>
                            Price: 1.00
                        </div>
                        <div class="col-md-3">
                            <h3>B2</h3>
                            100 Grand  
                            <br>
                            Price: 0.75 
                        </div>
                        <div class="col-md-3">
                            <h3>B3</h3>
                            Cow Tales
                            <br>
                            Price: 0.75 
                        </div>
                    </div>
                    <hr/>
                    <div class="row text-center">

                        <div class="col-md-3">
                            <h3>C1</h3>
                            The Oldest Bag of Lays
                            <br>
                            Price: 0.15
                        </div>
                        <div class="col-md-3">
                            <h3>C2</h3>
                            Generic Candy Bar #4  
                            <br>
                            Price: 0.50
                        </div>
                        <div class="col-md-3">
                            <h3>C3</h3>
                            An iPod
                            <br>
                            Price: 300.00
                        </div>
                    </div>
                </div>

                <div class="col-md-3" style="border: 1px solid;">
                    <div class="text-center"><strong>MAKE YOUR SELECTION</strong></div>
                    <form method="POST" action="results">
                        <fieldset>
                            <div class="row">

                                <div class="col-md-4">

                                    <button class="btn-block" type="button" value="A" id="A">
                                        A
                                    </button>
                                </div>
                                <div class="col-md-4">
                                    <button class="btn-block" type="button" value="B" id="B">
                                        B
                                    </button>
                                </div>
                                <div class="col-md-4">
                                    <button class="btn-block" type="button" value="C" id="C">
                                        C
                                    </button>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <button class="btn-block" type="button" value="1" id="1">
                                        1
                                    </button>
                                </div>
                                <div class="col-md-4">
                                    <button class="btn-block" type="button" value="2" id="2">
                                        2
                                    </button>
                                </div>
                                <div class="col-md-4">
                                    <button class="btn-block" type="button" value="3" id="3">
                                        3
                                    </button>
                                </div>

                            </div>
                            <br>
                            <div class="row text-center">
                                You have selected:<br>
                                <input class="col-xs-2" type="text" name="letters" id="letters" readonly style="border: none;"/>
                                <input class="col-xs-2" type="text" name="numbers" id="numbers" readonly style="border: none;" />
                            </div>
                            <div class="row text-center">
                                <br>You have <strong>$${moneyInserted}</strong> in the machine.

                            </div>
                            <div class="row">
                                <br>
                                <button class="btn-block btn-lg btn-primary" type="submit"  id="submit">VEND</button>

                            </div>

                        </fieldset>
                    </form>
 <!--                   
                                        <div id="candyOut" class="well well-lg text-center">
                    
                                            here is your candy
                                        </div>-->
                </div>
            </div>
        </div>

        <script>

            $("#A").click(function () {
                $("#letters").val("A");
            });

            $("#B").click(function () {
                $("#letters").val("B");
            });
            $("#C").click(function () {
                $("#letters").val("C");
            });

            $("#1").click(function () {
                $("#numbers").val("1");
            });
            $("#2").click(function () {
                $("#numbers").val("2");
            });
            $("#3").click(function () {
                $("#numbers").val("3");
            });
        </script>

        <!-- Placed at the end of the document so the pages load faster -->
        <!--        <script src="{pageContext.request.contextPath}/js/jquery-1.11.0.min.js"></script>-->
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

