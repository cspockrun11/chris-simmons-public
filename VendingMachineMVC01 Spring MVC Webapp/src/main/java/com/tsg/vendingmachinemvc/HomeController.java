/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.vendingmachinemvc;

import com.tsg.vendingmachinemvc.dao.VendingMachineDaoInMemImpl;
import com.tsg.vendingmachinemvc.dto.Candy;
import com.tsg.vendingmachinemvc.dto.Change;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.util.Scanner;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class HomeController {

    double moneyInserted;
    double changeDue;
    String candyName;
    Change change = new Change();
    final String DELIMITER = "::";
    DecimalFormat df = new DecimalFormat("#.00");

    VendingMachineDaoInMemImpl dao = new VendingMachineDaoInMemImpl();

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String displayHomePage() {
        return "index";
    }

    @RequestMapping(value = "vending", method = RequestMethod.POST)
    public String useTheVendingMachine(HttpServletRequest req) throws FileNotFoundException {
        //does not read from file - all hard-coded
        
        moneyInserted = Double.parseDouble(req.getParameter("userMoney"));
        req.setAttribute("moneyInserted", df.format(moneyInserted));
        return "vending";

    }

    @RequestMapping(value = "results", method = RequestMethod.POST)
    public String heyHeyDoThatVendingThing(HttpServletRequest req) {

        String letterChoice = req.getParameter("letters");
        String numberChoice = req.getParameter("numbers");

        String userChoice = letterChoice + numberChoice;

        switch (userChoice) {
            case "A1":
                //take money away
                changeDue = moneyInserted - 0.75;
                candyName = "Snickers";
                //minus 1 stock
                break;
            case "A2":
                //take money away
                changeDue = moneyInserted - 0.75;
                candyName = "Twix";
                //minus 1 stock
                break;
            case "A3":
                //take money away
                changeDue = moneyInserted - 1.00;
                candyName = "Skittles";
                break;
            case "B1":
                changeDue = moneyInserted - 1.00;
                candyName = "Kit-Kat";
                break;
            case "B2":
                changeDue = moneyInserted - 0.75;
                candyName = "100 Grand";
                break;
            case "B3":
                changeDue = moneyInserted - 0.75;
                candyName = "Cow Tales";
                break;
            case "C1":
                changeDue = moneyInserted - 0.15;
                candyName = "Oldest Bag of Lays";
                break;
            case "C2":
                changeDue = moneyInserted - 0.50;
                candyName = "Generic Candy Bar #4";
                break;
            case "C3":
                if (moneyInserted < 300.00) {
                    candyName = "nothing! You don't have enough money.";
                    changeDue = moneyInserted - 300.00;
                    break;
                } else {
                changeDue = moneyInserted - 300.00;
                candyName = "an iPod";
                break;
                }
            default:
                break;
        }
        
        change = dao.calculateChange(changeDue);
        
        
        int quarters = change.getQuartersReturned();
        int dimes = change.getDimesReturned();
        int nickels = change.getNickelsReturned();
        int pennies = change.getPenniesReturned();

        req.setAttribute("candyName", candyName);
        req.setAttribute("userChange", df.format(changeDue));
        req.setAttribute("quarters", quarters);
        req.setAttribute("dimes", dimes);
        req.setAttribute("nickels", nickels);
        req.setAttribute("pennies", pennies);
        
        return "results";
    }

}
