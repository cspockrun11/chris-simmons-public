/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.vendingmachinemvc.dto;

/**
 *
 * @author apprentice
 */
public class Change {

    private int quartersReturned;
    private int dimesReturned;
    private int nickelsReturned;
    private int penniesReturned;

    public int getQuartersReturned() {
        return quartersReturned;
    }

    public void setQuartersReturned(int quartersReturned) {
        this.quartersReturned = quartersReturned;
    }

    public int getDimesReturned() {
        return dimesReturned;
    }

    public void setDimesReturned(int dimesReturned) {
        this.dimesReturned = dimesReturned;
    }

    public int getNickelsReturned() {
        return nickelsReturned;
    }

    public void setNickelsReturned(int nickelsReturned) {
        this.nickelsReturned = nickelsReturned;
    }

    public int getPenniesReturned() {
        return penniesReturned;
    }

    public void setPenniesReturned(int penniesReturned) {
        this.penniesReturned = penniesReturned;
    }

}
