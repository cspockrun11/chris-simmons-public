/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.vendingmachinemvc.dao;

import com.tsg.vendingmachinemvc.dto.Candy;
import com.tsg.vendingmachinemvc.dto.Change;
import java.util.HashMap;

/**
 *
 * @author apprentice
 */
public class VendingMachineDaoInMemImpl implements VendingMachineDAO {
    
       
    HashMap<String, Candy> machineStock = new HashMap<>();

    @Override
    public void loadMachine(Candy candyInStock) {

        machineStock.put((candyInStock.getID()), candyInStock);
    }

    @Override
    public HashMap<String, Candy> populateMenu() {
        return machineStock;
    }

    @Override
    public Change calculateChange(double moneyInserted) {
        int quartersReturned;
        int dimesReturned;
        int nickelsReturned;
        int penniesReturned;

        
        quartersReturned = (int) Math.floor(moneyInserted / .25);

        moneyInserted = moneyInserted % .25;

        dimesReturned = (int) Math.floor(moneyInserted / .10);

        moneyInserted = moneyInserted % .10;

        nickelsReturned = (int) Math.floor(moneyInserted / .05);

        moneyInserted = moneyInserted % .05;

        penniesReturned = (int) Math.ceil(moneyInserted / .01);

        Change change = new Change();

        change.setQuartersReturned(quartersReturned);
        change.setDimesReturned(dimesReturned);
        change.setNickelsReturned(nickelsReturned);
        change.setPenniesReturned(penniesReturned);

        return change;
    }

}
