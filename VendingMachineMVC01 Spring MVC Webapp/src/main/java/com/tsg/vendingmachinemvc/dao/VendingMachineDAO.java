/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.vendingmachinemvc.dao;

import com.tsg.vendingmachinemvc.dto.Candy;
import com.tsg.vendingmachinemvc.dto.Change;
import java.util.HashMap;

/**
 *
 * @author apprentice
 */
public interface VendingMachineDAO {
    
        
    public void loadMachine(Candy candyInStock);
    
    public HashMap<String, Candy> populateMenu();
    
    public Change calculateChange(double moneyInserted);
    
       
}
