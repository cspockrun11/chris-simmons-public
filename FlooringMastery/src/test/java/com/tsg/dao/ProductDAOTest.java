/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dao;

import com.tsg.DAO.ProductDAO;
import com.tsg.DTO.Product;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class ProductDAOTest {

    public ProductDAOTest() {
    }

//    ProductDAO dao;
//
//    Product p1;
//    Product p2;
//    Product p3;

    @Before
    public void setUp() {

//        dao = new ProductDAO();
//
//        p1 = new Product();
//        p1.setProductName("Product 1");
//        p1.setCostPerSqFt(5.0);
//        p1.setLaborCostPerSqFt(10.0);
//
//        p2 = new Product();
//        p2.setProductName("Product 2");
//        p2.setCostPerSqFt(2.0);
//        p2.setLaborCostPerSqFt(4.0);
//
//        p3 = new Product();
//        p3.setProductName("Product 3");
//        p3.setCostPerSqFt(3.0);
//        p3.setLaborCostPerSqFt(6.0);

    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void addNewProductTest() {

//        dao.addNewProduct(p1);
//        dao.addNewProduct(p2);
//        dao.addNewProduct(p3);
//
//        //when i pull it out it should be the same thing
//        //arraylist should be added 3 more
//        Product result = dao.getProducts().get(0);
//        Product result2 = dao.getProducts().get(1);
//
//        Assert.assertEquals(p1, result);
//        Assert.assertEquals(p2, result2);
//
//        //see if 3 equals the number of entries that you specified up top
//        Assert.assertEquals(3, dao.getProducts().size());

    }

    @Test
    public void removeProductTest() {

//        dao.addNewProduct(p1);
//        dao.addNewProduct(p2);
//        dao.addNewProduct(p3);
//
//        dao.removeProduct(p2);
//
//        Assert.assertEquals(2, dao.getProducts().size());
//        Assert.assertNotEquals(p1, dao.getProducts().get(1));
//        Assert.assertEquals(p3, dao.getProducts().get(1));

    }

    @Test
    public void getProductsTest() {
//
//        ArrayList<Product> x = new ArrayList<>();
//        Assert.assertEquals(x, dao.getProducts());
//        Assert.assertTrue(dao.getProducts().isEmpty()); // haven't added anything
//
//        dao.addNewProduct(p1);
//        dao.addNewProduct(p2);
//        dao.addNewProduct(p3);
//
//        x.add(p1);
//        x.add(p2);
//        x.add(p3);
//
//        Assert.assertEquals(3, dao.getProducts().size());

    }

    @Test
    public void updateProductTest() {

//        ArrayList<Product> x = new ArrayList<>();
//
//        x.add(p1);
//        x.add(p2);
//
//        dao.addNewProduct(p1);
//        dao.updateProducts(p1);
//        dao.addNewProduct(p2);
//
//        Assert.assertNotEquals(x.get(0), x.get(1));
//        Assert.assertSame(x.get(0), dao.getProducts().get(0));
//        Assert.assertNotSame(p1, p2);

    }

    @Test
    public void loadProductsTest() {

//        ProductDAO ndao = new ProductDAO();
//
//        dao.addNewProduct(p1);
//        dao.addNewProduct(p2);
//        dao.addNewProduct(p3);
//
//        try {
//            dao.writeProducts();
//        } catch (IOException ex) {
//            fail();
//            Logger.getLogger(ProductDAOTest.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        try {
//            ndao.loadProducts();
//        } catch (FileNotFoundException ex) {
//            fail();
//            Logger.getLogger(ProductDAOTest.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        Assert.assertEquals(dao.getProducts(), ndao.getProducts());
    }

    @Test
    public void writeProductsTest() {
        
//        ProductDAO nndao = new ProductDAO();
//        
//        nndao.addNewProduct(p1);
//        nndao.addNewProduct(p2);
//        nndao.addNewProduct(p3);
//        
//        
//        
//        try {
//            dao.loadProducts();
//        } catch (FileNotFoundException ex) {
//            fail();
//            Logger.getLogger(ProductDAOTest.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
//        try {
//            nndao.writeProducts();
//        } catch (IOException ex) {
//            fail();
//            Logger.getLogger(ProductDAOTest.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
//        Assert.assertEquals(dao.getProducts().size(), nndao.getProducts().size());
        
    }
    
    
}
