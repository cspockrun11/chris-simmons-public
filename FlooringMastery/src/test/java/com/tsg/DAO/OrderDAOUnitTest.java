/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.DAO;

import com.tsg.DTO.Order;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class OrderDAOUnitTest {

    public OrderDAOUnitTest() {
    }

    OrderDAO dao;

    Order o1;
    Order o2;
    Order o3;

    @Before
    public void setUp() {

        dao = new OrderDAO();

        o1 = new Order();
        o1.setArea(20.5);
        o1.setCustomerName("Phil");
        o1.setFileNumber("03102016");
        o1.setLaborCost(15.0);
        o1.setMaterialCost(15.0);
        o1.setOrderNumber(1);
        o1.setTax(50.0);
        o1.setTotal(1000);

        o2 = new Order();
        o2.setArea(23);
        o2.setCustomerName("Lil");
        o2.setFileNumber("03102016");
        o2.setLaborCost(20.0);
        o2.setMaterialCost(20.0);
        o2.setOrderNumber(2);
        o2.setTax(50.0);
        o2.setTotal(2000);

    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void getOrderTest() {
        ArrayList<Order> x = new ArrayList<>();

//        x.add(o1);
//        x.add(o2);
//
//        dao.getOrders().add(o1);
//        dao.getOrders().add(o2);
//
//        Assert.assertEquals(x.size(), dao.getOrders().size());
//
//        x.add(o3);
//        dao.getOrders();
//
//        Assert.assertNotEquals(x.size(), dao.getOrders().size());

    }

    @Test
    public void addOrderTest() { //not necessary? there is no add order method

//        ArrayList<Order> x = new ArrayList<>();
//        x.add(o1);
//        x.add(o2);
//        x.add(o3);
//
//        dao.getOrders().add(o1);
//        dao.getOrders().add(o2);
//        dao.getOrders().add(o3);
//
//        Assert.assertEquals(x.size(), dao.getOrders().size());

    }

    @Test
    public void getDatesTest() {
//        ArrayList<String> x = new ArrayList<>();
//        x.add("03102016");
//
//        dao.getDates().add("03102016");
//
//        Assert.assertEquals(x.size(), dao.getDates().size());
//
//        x.add("03092016");
//        x.add("03082016");
//
//        dao.getDates().add("03092016");
//
//        Assert.assertNotEquals(x.size(), dao.getDates().size());
//
//        dao.getDates().add("03082016");
//
//        Assert.assertEquals(x.size(), dao.getDates().size());

    }

    @Test
    public void updateDatesTest() {
//        ArrayList<String> x = new ArrayList<>();
//
//        x.add("date1");
////        dao.getDates().add("date1");
//
//        dao.updateDates("date1");
//
//        Assert.assertEquals(x.size(), dao.getDates().size());
//
//        x.add("date2");
//        x.add("date3");
//
//        String userTest = "another date";
//
//        dao.updateDates("date2");
//
//        Assert.assertNotEquals(x.size(), dao.getDates().size());
//
//        dao.updateDates(userTest);
//
//        Assert.assertNotEquals(x, dao.getDates());
//
//        Assert.assertEquals(x.size(), dao.getDates().size());

    }

    @Test
    public void updateEditedDatesTest() {
//        ArrayList<String> x = new ArrayList<>();
//
//        x.add("Date 1");
//        x.add("Date 2");
//
//        String newDate = "Date 1";
//        String editDate = "Date1";
//        dao.getDates().add(newDate);
//        dao.updateEditedDates(editDate);
//        dao.getDates().add(editDate);
//
//        //truly a doomsday scenario
////        String nullDate = null;               //null entries are impossible because of 
////        dao.getDates().add(nullDate);         //try/catches in the Console IO
////        dao.updateEditedDates(nullDate);
//        Assert.assertEquals(x.get(0), dao.getDates().get(0));
//        Assert.assertNotSame(newDate, editDate);
//        Assert.assertNotEquals(x.get(0), dao.getDates().get(1));
////        Assert.assertNull(dao.getDates().get(2));
//        
//        Assert.assertNotEquals(x.size(), dao.getDates().size());

    }

}
