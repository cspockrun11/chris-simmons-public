/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.DAO;

import com.tsg.DTO.Tax;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class TaxDAOUnitTest {

    public TaxDAOUnitTest() {
    }
//
//    TaxDAO dao;
//
//    Tax t1;
//    Tax t2;
//    Tax t3;

    @Before
    public void setUp() {

//        dao = new TaxDAO();
//
//        t1 = new Tax();
//        t1.setState("AL");
//        t1.setTaxRate(6.5);
//
//        t2 = new Tax();
//        t2.setState("MO");
//        t2.setTaxRate(8.3);
//
//        t3 = new Tax();
//        t3.setState("CO");
//        t3.setTaxRate(4.75);
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void addNewStateTest() {

        //adds one element to arraylist (Tax object -t1)
//        dao.addNewState(t1);
//        //test passes if t1 is the same as the object returned from index '0'
//        //of array list using .get()    
//        assertEquals(t1, dao.getTaxes().get(0));
//
//        //adds two elements to arraylist (Tax objects - t2 & t3)
//        dao.addNewState(t2);
//        dao.addNewState(t3);
//        //test passes if t3 is the same as the object returned from index '2'
//        assertEquals(t3, dao.getTaxes().get(2));
//        //test passes if index of object t2 in arraylist is equal to 1
//        assertEquals(1, dao.getTaxes().indexOf(t2));

    }

    @Test
    public void getTaxesTest() {
        
//        ArrayList<Tax> temp = new ArrayList<>();
//        //test passes if empty arraylist created locally is equal to arraylist returned from getTaxes()
//        assertEquals(temp, dao.getTaxes());
//        //test passes if arraylist returned from getTaxes() is empty
//        assertTrue(dao.getTaxes().isEmpty());
//        
//        //tax objects t1, t2 and t3 are added to arraylist using addNewState()
//        dao.addNewState(t1);
//        dao.addNewState(t2);
//        dao.addNewState(t3);
//        //tax objects t1, t2 and t3 are added to local arraylist
//        temp.add(t1);
//        temp.add(t2);
//        temp.add(t3);
//        
//        //test passes if size of arraylist in DAO is equal to 3
//        assertEquals(3, dao.getTaxes().size());
//        //test passes if object from index 2 of local arraylist is equal to
//        //object from index 2 of dao arraylist using getTaxes()
//        assertEquals(temp.get(2), dao.getTaxes().get(2));

    }

    @Test
    public void removeStateTest() {
//        //add objects t1, t2 and t3 to arraylist using addNewState()
//        dao.addNewState(t1);
//        dao.addNewState(t2);
//        dao.addNewState(t3);
//        //delete object t2 from arraylist using removeState()
//        dao.removeState(t2);
//        //test passes if size of arraylist is equal to 2
//        assertEquals(2, dao.getTaxes().size());
//        //test passes if object retured from index 1 is not equal to t2
//        //index 1 is the initial position of object t2 in the arraylist
//        assertNotEquals(t2, dao.getTaxes().get(1));
//        //test passes if object returned from index 1 is equal to t3
//        //index 1 is the new position of object t3 (after deleting t2)
//        assertEquals(t3, dao.getTaxes().get(1));
//        
//        //objects t1 and t3 are deleted from arraylist using removeState()
//        dao.removeState(t1);
//        dao.removeState(t3);
//        //test passes if arraylist isempty returns true
//        assertTrue(dao.getTaxes().isEmpty());

    }

    @Test
    public void updateTaxesTest() {
//        //add objects t1, t2 and t3 to arraylist using addNewState()
//        dao.addNewState(t1);
//        dao.addNewState(t2);
//        dao.addNewState(t3);
//        //Create a new tax object with state name same as t3 and new tax rate
//        Tax t4 = new Tax();
//        t4.setState("CO");
//        t4.setTaxRate(9.6);
//        //use updateTaxes to update arraylist
//        dao.updateTaxes(t4);
//        //test passes if object t3 is not equal to object returned from index 2 of arraylist
//        assertNotEquals(t3, dao.getTaxes().get(2));
//        //test passes if tax rate of object returned from arraylist index 2 is equal to 9.6      
//        assertEquals(9.6, dao.getTaxes().get(2).getTaxRate(), 0);       
        
    }
    
    @Test
    public void writeLoadTaxesTest() {
        //add new objects t1, t2 and t3 to arraylist in dao using addNewState()
//        dao.addNewState(t1);
//        dao.addNewState(t2);
//        dao.addNewState(t3);
//        
//        //new DAO object 'tdao' created
//        TaxDAO tdao = new TaxDAO();
//        
//        //write dao arraylist data into 'TaxFile.txt'
//        try {
//            dao.writeTaxes();
//        } catch (IOException ex) {
//            fail();
//            Logger.getLogger(TaxDAOUnitTest.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
        //load 'TaxFile.txt' data and store into arraylist in 'tdao'
//        try {
//            tdao.loadTaxes();
//        } catch (FileNotFoundException ex) {
//            fail();
//            Logger.getLogger(TaxDAOUnitTest.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
//        //get individual elements of the arraylist in 'tdao' and store in tax objects        
//        Tax readT1 = tdao.getTaxes().get(0);
//        Tax readT2 = tdao.getTaxes().get(1);
//        Tax readT3 = tdao.getTaxes().get(2);
//        
//        //tests pass if, tax objects from 'dao' arraylist are equal to tax objects from 'tdao'
//        assertEquals(t1, readT1);
//        assertEquals(t2, readT2);
//        assertEquals(t3, readT3);
//        //test passes if, arraylist from 'dao' is equal to arraylist from 'tdao'
//        assertEquals(dao.getTaxes(), tdao.getTaxes());
    }
}
