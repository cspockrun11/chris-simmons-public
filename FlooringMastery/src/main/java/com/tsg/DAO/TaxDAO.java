/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.DAO;

import com.tsg.DTO.Product;
import com.tsg.DTO.Tax;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class TaxDAO implements TaxInterface {

    ArrayList<Tax> taxes = new ArrayList<>();
    final String DELIMITER = ",";

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + Objects.hashCode(this.taxes);
        hash = 71 * hash + Objects.hashCode(this.DELIMITER);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TaxDAO other = (TaxDAO) obj;
        if (!Objects.equals(this.DELIMITER, other.DELIMITER)) {
            return false;
        }
        if (!Objects.equals(this.taxes, other.taxes)) {
            return false;
        }
        return true;
    }

    @Override
    public ArrayList<Tax> getTaxes() {
        return taxes;
    }

    @Override
    public void loadTaxes() throws FileNotFoundException {

        Scanner load = new Scanner(new BufferedReader(new FileReader("TaxFile.txt")));

        String currentLine;
        String[] currentTokens;
        // ArrayList<Tax> tempList = new ArrayList<>();

        while (load.hasNext()) {
            currentLine = load.nextLine();
            currentTokens = currentLine.split(DELIMITER);

            Tax t = new Tax();

            t.setState(currentTokens[0]);
            t.setTaxRate(Double.parseDouble(currentTokens[1]));

            taxes.add(t);

        }

        load.close();
//        return tempList;

    }

    @Override
    public void addNewState(Tax newTax) {
        taxes.add(newTax);
    }

    @Override
    public void removeState(Tax t) {
        Iterator<Tax> iter = taxes.iterator();
        Tax toDelete = new Tax();
        while (iter.hasNext()) {
            Tax temp = iter.next();
            if (temp.getState().toLowerCase().equals(t.getState().toLowerCase())) {
                toDelete = temp;
            }
        }
        taxes.remove(toDelete);
    }

    @Override
    public void updateTaxes(Tax t) {
        for (Tax temp : taxes) {
            if (temp.getState().toLowerCase().equals(t.getState().toLowerCase())) {
                taxes.set(taxes.indexOf(temp), t);
            }
        }
    }

    @Override
    public void writeTaxes() throws IOException {
        PrintWriter writer = new PrintWriter(new FileWriter("TaxFile.txt"));
        for (Tax t : taxes) {
            writer.println(t.getState()+ DELIMITER + t.getTaxRate());

            writer.flush();

        }

        writer.close();
    }

}
