/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.DAO;

import com.tsg.DTO.Product;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ProductDAO implements ProductInterface {

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + Objects.hashCode(this.products);
        hash = 19 * hash + Objects.hashCode(this.DELIMITER);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProductDAO other = (ProductDAO) obj;
        if (!Objects.equals(this.DELIMITER, other.DELIMITER)) {
            return false;
        }
        if (!Objects.equals(this.products, other.products)) {
            return false;
        }
        return true;
    }

    ArrayList<Product> products = new ArrayList<>();
    final String DELIMITER = ",";

    @Override
    public void loadProducts() throws FileNotFoundException {

        Scanner load = new Scanner(new BufferedReader(new FileReader("ProductsFile.txt")));

        String currentLine;
        String[] currentTokens;
        //ArrayList<Product> tempList = new ArrayList<>();

        while (load.hasNext()) {
            currentLine = load.nextLine();
            currentTokens = currentLine.split(DELIMITER);

            Product p = new Product();
            p.setProductName(currentTokens[0]);
            p.setCostPerSqFt(Double.parseDouble(currentTokens[1]));
            p.setLaborCostPerSqFt(Double.parseDouble(currentTokens[2]));

            products.add(p);

        }

        load.close();
        //return tempList;

    }

    @Override
    public ArrayList<Product> getProducts() {

        return products;
    }

    @Override
    public void addNewProduct(Product newProduct) {
        products.add(newProduct);
    }

    @Override
    public void removeProduct(Product p) {
        Iterator<Product> iter = products.iterator();
        Product toDelete = new Product();
        while (iter.hasNext()) {
            Product temp = iter.next();
            if (temp.getProductName().toLowerCase().equals(p.getProductName().toLowerCase())) {
                toDelete = temp;
            }
        }
        products.remove(toDelete);
    }


    @Override
    public void updateProducts(Product p) {
        for (Product temp : products) {
            if (temp.getProductName().toLowerCase().equals(p.getProductName().toLowerCase())) {
                products.set(products.indexOf(temp), p);
            }
        }
    }

    @Override
    public void writeProducts() throws IOException {
        PrintWriter writer = new PrintWriter(new FileWriter("ProductsFile.txt"));
        for (Product p : products) {
            writer.println(p.getProductName() + DELIMITER + p.getCostPerSqFt()
                    + DELIMITER + p.getLaborCostPerSqFt());

            writer.flush();

        }

        writer.close();
    }

}
