/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.DAO;

import com.tsg.DTO.Order;
import com.tsg.DTO.Product;
import com.tsg.DTO.Tax;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class OrderDAO implements OrderInterface {

    final String DELIMITER = ",";
    ArrayList<String> dates = new ArrayList<>();
    ArrayList<Order> orders = new ArrayList<>();
    ArrayList<String> filesReadyToBeDeleted = new ArrayList<>();

    @Override
    public ArrayList<String> getDates() {
        return dates;
    }

    @Override
    public ArrayList<Order> getOrders() {
        return orders;
    }

    @Override
    public void updateDates(String userDate) {
        dates.add(userDate);
    }

    @Override
    public void updateOrders(Order temp) {
        orders.add(temp);
    }

    @Override
    public void updateEditedDates(String newDate) {
        boolean dateNotFound = true;
        for (String d : dates) {
            if (d.equals(newDate)) {
                dateNotFound = false;
            }
        }

        if (dateNotFound) {
            updateDates(newDate);
        }

        boolean delete = false;
        String deleteDate = "";
        for (String d : dates) {
            boolean dateFound = false;
            for (Order o : orders) {
                if (d.equals(o.getFileNumber())) {
                    dateFound = true;
                }
            }
            if (dateFound == false) {
                delete = true;
                deleteDate = d;
            }
        }
        
        if (delete) {
            dates.remove(deleteDate);
            filesReadyToBeDeleted.add(deleteDate);
        }
    }

    @Override
    public void updateDeletedDates(String userDate, Order temp) {
        if (orders.isEmpty()) {
            dates.removeAll(dates);
        }
        boolean notFound = true;
        for (Order o : orders) {
            if (o.getFileNumber().equals(userDate)) {
                notFound = false;
            }
        }
        if (notFound) {
            dates.remove(userDate);
            filesReadyToBeDeleted.add(userDate);
        }
    }

    @Override
    public void writeDates() throws IOException {
        PrintWriter writer = new PrintWriter(new FileWriter("Dates.txt"));
        for (String d : dates) {
            writer.println(d);
            writer.flush();
        }
        writer.close();
    }

    @Override
    public void loadDates() throws FileNotFoundException {
        Scanner loader = new Scanner(new BufferedReader(new FileReader("Dates.txt")));
        String currentLine;
        while (loader.hasNext()) {
            currentLine = loader.next();
            dates.add(currentLine);
        }

        loader.close();
    }
    @Override
    public void updateEditedOrder(String userDate, int orderNumber, Order o) {
        for (Order temp : orders) {
            if (temp.getOrderNumber() == orderNumber && temp.getFileNumber().equals(userDate)) {
                orders.set(orders.indexOf(temp), o);
            }
        }
    }

    @Override
    public void updateDeletedOrder(String userDate, int orderNumber, Order o) {
        Iterator<Order> iter = orders.iterator();
        Order toDelete = new Order();
        while (iter.hasNext()) {
            Order temp = iter.next();
            if (temp.getOrderNumber() == orderNumber && temp.getFileNumber().equals(userDate)) {
                toDelete = temp;
            }
        }
        orders.remove(toDelete);
    }

    @Override
    public void writeOrders() throws IOException {
        for (String d : dates) {
            String fileName = "Orders_" + d + ".txt";
            PrintWriter writer = new PrintWriter(new FileWriter(fileName));
            for (Order o : orders) {
                if (o.getFileNumber().equals(d)) {
                    Tax t = o.getTaxInfo();
                    Product p = o.getType();
                    writer.println(o.getOrderNumber() + DELIMITER + o.getCustomerName()
                            + DELIMITER + t.getState() + DELIMITER + t.getTaxRate()
                            + DELIMITER + o.getArea() + DELIMITER + p.getProductName()
                            + DELIMITER + p.getCostPerSqFt() + DELIMITER + p.getLaborCostPerSqFt()
                            + DELIMITER + o.getMaterialCost() + DELIMITER + o.getLaborCost()
                            + DELIMITER + o.getTax() + DELIMITER + o.getTotal() + DELIMITER + o.getFileNumber());

                    writer.flush();

                }
            }

            writer.close();
        }

    }

    @Override
    public void loadOrders() throws FileNotFoundException {
        for (String d : dates) {
            String fileName = "Orders_" + d + ".txt";
            Scanner loader = new Scanner(new BufferedReader(new FileReader(fileName)));
            String currentLine;
            String[] currentTokens;

            while (loader.hasNext()) {
                Order o = new Order();
                currentLine = loader.nextLine();
                currentTokens = currentLine.split(DELIMITER);

                o.setOrderNumber(Integer.parseInt(currentTokens[0]));
                o.setCustomerName(currentTokens[1]);
                Tax t = new Tax();
                t.setState(currentTokens[2]);
                t.setTaxRate(Double.parseDouble(currentTokens[3]));
                o.setTaxInfo(t);
                o.setArea(Double.parseDouble(currentTokens[4]));
                Product p = new Product();
                p.setProductName(currentTokens[5]);
                p.setCostPerSqFt(Double.parseDouble(currentTokens[6]));
                p.setLaborCostPerSqFt(Double.parseDouble(currentTokens[7]));
                o.setType(p);
                o.setMaterialCost(Double.parseDouble(currentTokens[8]));
                o.setLaborCost(Double.parseDouble(currentTokens[9]));
                o.setTax(Double.parseDouble(currentTokens[10]));
                o.setTotal(Double.parseDouble(currentTokens[11]));
                o.setFileNumber(currentTokens[12]);

                orders.add(o);

            }

            loader.close();

        }
    }

    @Override
    public void deleteEmptyFiles() {
        if (filesReadyToBeDeleted.isEmpty()) {
            
        }else {
            for(String deleteFileName : filesReadyToBeDeleted) {
                File f = new File("Orders_" + deleteFileName + ".txt");
                f.delete();
            }
            filesReadyToBeDeleted.clear();
        }
    }

}
