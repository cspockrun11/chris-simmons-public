/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.DAO;

import com.tsg.DTO.Tax;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public interface TaxInterface {

    void addNewState(Tax newTax);

    boolean equals(Object obj);

    ArrayList<Tax> getTaxes();

    int hashCode();

    void loadTaxes() throws FileNotFoundException;

    void removeState(Tax t);

    void updateTaxes(Tax t);

    void writeTaxes() throws IOException;
    
}
