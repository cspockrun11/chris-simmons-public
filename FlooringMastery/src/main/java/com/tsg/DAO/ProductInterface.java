/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.DAO;

import com.tsg.DTO.Product;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public interface ProductInterface {

    void addNewProduct(Product newProduct);

    boolean equals(Object obj);

    ArrayList<Product> getProducts();

    int hashCode();

    void loadProducts() throws FileNotFoundException;

    void removeProduct(Product p);

    void updateProducts(Product p);

    void writeProducts() throws IOException;
    
}
