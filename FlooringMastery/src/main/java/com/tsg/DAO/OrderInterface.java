/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.DAO;

import com.tsg.DTO.Order;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public interface OrderInterface {

    void deleteEmptyFiles();

    ArrayList<String> getDates();

    ArrayList<Order> getOrders();

    void loadDates() throws FileNotFoundException;

    void loadOrders() throws FileNotFoundException;

    void updateDates(String userDate);

    void updateDeletedDates(String userDate, Order temp);

    void updateDeletedOrder(String userDate, int orderNumber, Order o);

    void updateEditedDates(String newDate);

    void updateEditedOrder(String userDate, int orderNumber, Order o);

    void updateOrders(Order temp);

    void writeDates() throws IOException;

    void writeOrders() throws IOException;
    
}
