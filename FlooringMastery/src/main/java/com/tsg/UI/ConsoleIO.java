/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.UI;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class ConsoleIO {

    private Scanner sc = new Scanner(System.in);

    public void print(String msg) {
        System.out.print(msg);
    }

    public String readString(String prompt) {
        Scanner mySc = new Scanner(System.in);
        System.out.println(prompt);
        String replacedStringWithoutCommas = mySc.nextLine();
        return replacedStringWithoutCommas.replace(",", ";");
    }

    public int readInt(String prompt) {
        boolean badInput;
        int result = 0;
        do {
            try {
                System.out.println(prompt);
                result = sc.nextInt();
                badInput = false;
            } catch (InputMismatchException ime) {
                badInput = true;
                System.out.println("Invalid input");
                sc.nextLine();
            }
        } while (badInput);
        return result;
    }

    public int readInt(String prompt, int min, int max) {
        int result;
        do {
            result = readInt(prompt);
            if (result < min || result > max) {
                System.out.println("Input should be within range: " + min + "-" + max);
            }
        } while (result < min || result > max);

        return result;
    }

    public long readLong(String prompt) {
        boolean badInput;
        long result = 0;
        do {
            try {
                System.out.println(prompt);
                result = sc.nextLong();
                badInput = false;
            } catch (InputMismatchException ime) {
                badInput = true;
                System.out.println("Invalid input");
                sc.nextLine();
            }
        } while (badInput);

        return result;
    }

    public long readLong(String prompt, long min, long max) {
        long result;
        do {
            result = readLong(prompt);
            if (result < min || result > max) {
                System.out.println("Input should be within range: " + min + "-" + max);
            }
        } while (result < min || result > max);

        return result;
    }

    public float readFloat(String prompt) {
        boolean badInput;
        float result = 0;
        do {
            try {
                System.out.println(prompt);
                result = sc.nextFloat();
                badInput = false;
            } catch (InputMismatchException ime) {
                badInput = true;
                System.out.println("Invalid input");
                sc.nextLine();
            }
        } while (badInput);

        return result;
    }

    public float readFloat(String prompt, float min, float max) {
        float result;
        do {
            result = readFloat(prompt);
            if (result < min || result > max) {
                System.out.println("Input should be within range: " + min + "-" + max);
            }
        } while (result < min || result > max);

        return result;
    }

    public double readDouble(String prompt) {
        boolean badInput;
        double result = 0;
        do {
            try {
                System.out.println(prompt);
                result = sc.nextDouble();
                badInput = false;
            } catch (InputMismatchException ime) {
                badInput = true;
                System.out.println("Invalid input");
                sc.nextLine();
            }
        } while (badInput);

        return result;
    }

    public double readDouble(String prompt, double min, double max) {
        double result;
        do {
            result = readDouble(prompt);
            if (result < min || result > max) {
                System.out.println("Input should be within range: " + min + "-" + max);
            }
        } while (result < min || result > max);

        return result;
    }

    public String readDate() {

        Scanner newSc = new Scanner(System.in);
        SimpleDateFormat format1 = new SimpleDateFormat("MMddyyyy");

        boolean formatCorrect = false;

        String tempUserDate;
        DateFormat inputFormatter = new SimpleDateFormat("MM-dd-yyyy");
        Calendar cal1 = Calendar.getInstance();
        String todaysDate = inputFormatter.format(cal1.getTime());
        
        do {
            System.out.println("\nPlease enter the date for the order (MMDDYYYY), \nor press ENTER for today's date:" + todaysDate);

            tempUserDate = newSc.nextLine();

            if (tempUserDate.equals("")) {
                Calendar cal = Calendar.getInstance();
                String formatted = format1.format(cal.getTime());
                tempUserDate = formatted;
                //return tempUserDate;
                formatCorrect = true;
            }
            try {
                int parsedDate = Integer.parseInt(tempUserDate);
                if (tempUserDate.length() == 8) {

                    String s1 = tempUserDate.substring(0, 2);
                    String s2 = tempUserDate.substring(2, 4);
                    String s3 = tempUserDate.substring(4, 8);

                    int ps1 = Integer.parseInt(s1);
                    int ps2 = Integer.parseInt(s2);
                    int ps3 = Integer.parseInt(s3);

                    if ((ps1 >= 1 && ps1 <= 12) && (ps2 >= 1 && ps2 <= 31) && (ps3 >= 1900 && ps3 <= 2016)) //2016
                    {
                        if (((ps1 == 1) || (ps1 == 3 || (ps1 == 5) || (ps1 == 7) || (ps1 == 8) || (ps1 == 10) || (ps1 == 12)) && (ps2 >= 1 && ps2 <= 31))) {
                            formatCorrect = true;
                        } else if (((ps1 == 4) || (ps1 == 6 || (ps1 == 9) || (ps1 == 11)) && ((ps2 >= 1 && ps2 <= 30)))) {
                            formatCorrect = true;
                        } else if ((ps1 == 2)) {
                            boolean isLeapYear = ((ps3 % 4 == 0) && (ps3 % 100 != 0) || (ps3 % 400 == 0));
                            if ((isLeapYear == true) && (ps2 >= 1 && ps2 <= 29)) {
                                formatCorrect = true;
                            } else if ((isLeapYear == false) && (ps2 >= 1 && ps2 <= 28)) {
                                formatCorrect = true;
                            } else if ((isLeapYear == false) && (ps2 >= 29)) {
                                System.out.println("You can't have " + ps2 + " days in " + getMonth(ps1) + ". " + ps3 + " is not a leap year.");
                            } else {
                                System.out.println("You can't have " + ps2 + " days in " + getMonth(ps1) + ".");
                            }
                        } else {
                            System.out.println("You can't have " + ps2 + " days in " + getMonth(ps1) + ".");
                        }
                    } else {
                        System.out.println("MM means 'months', from 01-12.\nDD is 'days', from 01-31, \nand YYYY is 'years'"
                                + " from 1900-2016.");
                    }

                } else {
                    System.out.println("Please format your date in the following format: MMDDYYYY");
                }

            } catch (NumberFormatException e) {
                System.out.println("Please format your date in the following format: MMDDYYYY"); //is trash

            }
        } while (formatCorrect == false);

        return tempUserDate;

    }

    public String readEditedDate(String fileNumber) {
        Scanner newSc = new Scanner(System.in);

        boolean formatCorrect = false;

        String tempUserDate;

        do {
            System.out.println("\nEnter date for the order (MMDDYYYY): [Press ENTER to skip]");

            tempUserDate = newSc.nextLine();

            if (tempUserDate.equals("")) {
                tempUserDate = fileNumber;
                formatCorrect = true;
            } else {

                try {
                    int parsedDate = Integer.parseInt(tempUserDate);
                    if (tempUserDate.length() == 8) {

                        String s1 = tempUserDate.substring(0, 2);
                        String s2 = tempUserDate.substring(2, 4);
                        String s3 = tempUserDate.substring(4, 8);

                        int ps1 = Integer.parseInt(s1);
                        int ps2 = Integer.parseInt(s2);
                        int ps3 = Integer.parseInt(s3);

                        if ((ps1 >= 1 && ps1 <= 12) && (ps2 >= 1 && ps2 <= 31) && (ps3 >= 1900 && ps3 <= 2016)) //2016
                        {
                            if (((ps1 == 1) || (ps1 == 3 || (ps1 == 5) || (ps1 == 7) || (ps1 == 8) || (ps1 == 10) || (ps1 == 12)) && (ps2 >= 1 && ps2 <= 31))) {
                                formatCorrect = true;
                            } else if (((ps1 == 4) || (ps1 == 6 || (ps1 == 9) || (ps1 == 11)) && ((ps2 >= 1 && ps2 <= 30)))) {
                                formatCorrect = true;
                            } else if ((ps1 == 2)) {
                                boolean isLeapYear = ((ps3 % 4 == 0) && (ps3 % 100 != 0) || (ps3 % 400 == 0));
                                if ((isLeapYear == true) && (ps2 >= 1 && ps2 <= 29)) {
                                    formatCorrect = true;
                                } else if ((isLeapYear == false) && (ps2 >= 1 && ps2 <= 28)) {
                                    formatCorrect = true;
                                } else if ((isLeapYear == false) && (ps2 >= 29)) {
                                    System.out.println("You can't have " + ps2 + " days in " + getMonth(ps1) + ". " + ps3 + " is not a leap year.");
                                } else {
                                    System.out.println("You can't have " + ps2 + " days in " + getMonth(ps1) + ".");
                                }
                            } else {
                                System.out.println("You can't have " + ps2 + " days in " + getMonth(ps1) + ".");
                            }
                        } else {
                            System.out.println("MM means 'months', from 01-12.\nDD is 'days', from 01-31, \nand YYYY is 'years'"
                                    + " from 1900-2016.");
                        }

                    } else {
                        System.out.println("Please format your date in the following format: MMDDYYYY");
                    }

                } catch (NumberFormatException e) {
                    System.out.println("Please format your date in the following format: MMDDYYYY"); //is trash

                }
            }
        } while (formatCorrect == false);

        return tempUserDate;
    }

    public String getMonth(int ps1) {
        String returnMonth = "";
        switch (ps1) {
            case 1:
                returnMonth = "January";
                break;
            case 2:
                returnMonth = "February";
                break;
            case 3:
                returnMonth = "March";
                break;
            case 4:
                returnMonth = "April";
                break;
            case 5:
                returnMonth = "May";
                break;
            case 6:
                returnMonth = "June";
                break;
            case 7:
                returnMonth = "July";
                break;
            case 8:
                returnMonth = "August";
                break;
            case 9:
                returnMonth = "September";
                break;
            case 10:
                returnMonth = "October";
                break;
            case 11:
                returnMonth = "November";
                break;
            case 12:
                returnMonth = "December";
                break;
            default:
                break;
        }
        return returnMonth;
    }
}
