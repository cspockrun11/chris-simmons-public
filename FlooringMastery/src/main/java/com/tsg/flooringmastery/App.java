/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.flooringmastery;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * //MAAZ QURESHI AND CHRIS SIMMONS
 *
 * @author apprentice
 */
public class App {
    
    public static void main(String[] args) throws FileNotFoundException, IOException {
        
        OrderController controller = new OrderController();
        
        controller.run();
        
    }
    
}
