/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.flooringmastery;

import com.tsg.DAO.OrderDAO;
import com.tsg.DAO.ProductDAO;
import com.tsg.DAO.TaxDAO;
import com.tsg.DTO.Order;
import com.tsg.DTO.Product;
import com.tsg.DTO.Tax;
import com.tsg.UI.ConsoleIO;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * MAAZ QURESHI AND CHRIS SIMMONS
 *
 * @author apprentice
 */
public class OrderController {

    ConsoleIO con = new ConsoleIO();
    OrderDAO store = new OrderDAO();
    TaxDAO taxdao = new TaxDAO();
    ProductDAO prodao = new ProductDAO();

    boolean testMode;
    DecimalFormat df = new DecimalFormat("#.00");

    /**
     * *
     * Loads config file, OrderDAO, TaxDAO, ProductDAO, and keeps the main menu
     * live between all methods. NOTE: Save-to-file will only occur when the
     * user selects "SAVE" menu 5, or when Quitting (6) and inputting Y when
     * prompted. Otherwise, no files will be written to.
     *
     */
    public void run() {

        try {
            loadConfig();
            store.loadDates();
            taxdao.loadTaxes();
            prodao.loadProducts();
            store.loadOrders();
            boolean keepGoing = true;

            while (keepGoing == true) {
                printMenu();

                int userChoice;

                if (testMode) {
                    userChoice = con.readInt("\nPlease select a menu option:", 1, 12);
                } else {
                    userChoice = con.readInt("\nPlease select a menu option:", 1, 6);
                }

                switch (userChoice) {
                    case 1:
                        con.print("\nDisplaying all orders...");
                        displayAllOrders();
                        break;
                    case 2:
                        con.print("\nAdding order...");
                        addOrder();
                        break;
                    case 3:
                        con.print("\nEditing order...");
                        editOrder();
                        break;
                    case 4:
                        con.print("\nRemoving order...");
                        removeOrder();
                        break;
                    case 5:
                        con.print("\nSaving order...");
                        saveOrder();
                        break;
                    case 6:
                        if (testMode == false) {
                            String userSure = con.readString("Do you want to save before exiting?"
                                    + "\nAny unsaved work will be lost. (Y/N)");
                            if (userSure.equals("") || userSure.toLowerCase().equals("y")) {
                                con.print("Saving work...");
                                saveOrder();
                            } else {
                                con.print("Not saving...");
                            }
                        }
                        con.print("\nQuitting...\nThank you for using Flooring Manager.");
                        keepGoing = false;
                        break;
                    case 7:
                        con.print("\nAdding new product...");
                        createProduct();
                        break;
                    case 8:
                        con.print("\nEditing existing production information...");
                        editProduct();
                        break;
                    case 9:
                        con.print("\nDeleting existing product...");
                        deleteProduct();
                        break;
                    case 10:
                        con.print("\nAdding new state...");
                        createState();
                        break;
                    case 11:
                        con.print("\nEditing existing state tax information...");
                        editState();
                        break;
                    case 12:
                        con.print("\nDeleting state tax information");
                        deleteState();
                        break;
                    default:
                        con.print("\nTry Again...");
                }

            }
        } catch (FileNotFoundException e) {
            con.print("ERROR: Missing program files.\nCheck if one of the following files is missing from the project folder: \"Dates.txt\", \"ProductsFile.txt\" or \"TaxFile.txt\"");
        } catch (IOException e) {
            con.print("ERROR: Something's wrong...");
        }

    }

    /**
     * *
     * Displays Main Menu for users. Does not include a prompt.
     */
    private void printMenu() {

        con.print("\n***************************************");
        con.print("\n*\tWELCOME TO FLOORING MANAGER");
        con.print("\n*\t1. Display All Orders");
        con.print("\n*\t2. Add New Order");
        con.print("\n*\t3. Edit Order");
        con.print("\n*\t4. Remove Order");
        con.print("\n*\t5. Save Current Work");
        con.print("\n*\t6. Quit");
        if (testMode == false) {
            con.print("\n***************************************");
        } else {
            con.print("\n*\tTEST MODE OPTIONS:");
            con.print("\n*\t7. Add New Product");
            con.print("\n*\t8. Edit Product Information");
            con.print("\n*\t9. Delete Existing Product");
            con.print("\n*\t10. Add New State");
            con.print("\n*\t11. Edit State Tax Information");
            con.print("\n*\t12. Delete Existing State Information");
            con.print("\n***************************************");
        }
    }

    /**
     * *
     * Displays all current (saved and unsaved) Orders in numerical order by
     * orderNumber.
     */
    private void displayAllOrders() {

        ArrayList<String> dates = store.getDates();
        if (dates.isEmpty()) {
            con.print("\nStore records are empty. Create a new date entry before placing an order.\n");
            con.readString("\nPress ENTER to return to main menu...");

        } else {
            displayDates();

            String userDate = con.readDate();

            ArrayList<Order> orders = store.getOrders();

            int max = 0;
            int min = Integer.MAX_VALUE;

            for (Order o : orders) {
                if (o.getOrderNumber() > max) {
                    max = o.getOrderNumber();
                }
                if (o.getOrderNumber() < min) {
                    min = o.getOrderNumber();
                }

            }
            int x = min;
            boolean dateDoesNotExist = true;
            while (x <= max) {
                for (Order o : orders) {
                    if (o.getOrderNumber() == x) {

                        if (o.getFileNumber().equals(userDate)) {

                            dateDoesNotExist = false;
                            Tax t2 = o.getTaxInfo();
                            Product p = o.getType();
                            con.print("\nOrder Number:\t\t" + o.getOrderNumber()
                                    + "\nCustomer Name:\t\t" + o.getCustomerName()
                                    + "\nTax info:\t\t" + t2.getTaxRate() + "%"
                                    + "\nState:\t\t\t" + t2.getState()
                                    + "\nArea:\t\t\t" + o.getArea()
                                    + "\nProduct name:\t\t" + p.getProductName()
                                    + "\nCost/sq.ft.:\t\t$" + df.format(p.getCostPerSqFt())
                                    + "\nLabor Cost/sq.ft.:\t$" + df.format(p.getLaborCostPerSqFt())
                                    + "\nMaterial Cost:\t\t$" + df.format(o.getMaterialCost())
                                    + "\nLabor cost:\t\t$" + df.format(o.getLaborCost())
                                    + "\nTax:\t\t\t$" + df.format(o.getTax())
                                    + "\n--------------------------------"
                                    + "\nTOTAL:\t\t\t$" + df.format(o.getTotal()) + "\n--------------------------------\n");
                        }
                    }

                }
                x++;
            }

            if (dateDoesNotExist) {
                con.print("\nNo orders found for that date. Create a new order by selecting 2 in the main menu.");
                con.readString("\nPress ENTER to return to main menu...");
            }
        }
    }

    /**
     * *
     *
     * Begins process of adding a new user-inputted Order into the system.
     * "userDate" is a key component of file reading/writing, as the program
     * structure relies on userDates as file names. NOTE: new Orders are not
     * saved to a file until the user specifies he/she wants to do so, either by
     * using the "Save" function in menu option 5, or by choosing to save after
     * selecting "Quit" (6).
     *
     */
    private void addOrder() {

        String userDate = con.readDate();

        Order temp = getNewOrderData(userDate);

        con.print("\n");
        con.print("ORDER RECEIPT - YOUR ORDER IS NOT YET PLACED\n");
        con.print("Date: \t\t" + userDate.substring(0, 2) + "-" + userDate.substring(2, 4) + "-" + userDate.substring(4, 8) + "\n");
        con.print("Customer Name: \t" + temp.getCustomerName() + "\n");
        con.print("Area: \t\t" + df.format(temp.getArea()) + "sq.ft.\n");
        con.print("Material Cost: \t" + "$" + df.format(temp.getMaterialCost()) + "\n");
        con.print("Labor Cost: \t" + "$" + df.format(temp.getLaborCost()) + "\n");
        con.print("Tax: \t\t" + "$" + df.format(temp.getTax()) + "\n");
        con.print("TOTAL: \t\t" + "$" + df.format(temp.getTotal()) + "\n");

        ArrayList<String> dates = store.getDates();
        if (con.readString("\nDo you want to proceed with your order? Enter Y/N").toLowerCase().equals("y")) {
            store.updateOrders(temp);
            if (!dates.contains(userDate)) {
                store.updateDates(userDate);
            }
            con.print("\nOrder was successfully added. Select 5 in the main menu to save your progress.");
            con.readString("\nPress ENTER to return to main menu...");
        } else {
            con.print("\nOrder was cancelled.");
            con.readString("\nPress ENTER to return to main menu...");
        }

    }

    /**
     * *
     * Allows user to edit individual existing Orders, whether saved or unsaved.
     * Changes will not be written to file until user selects SAVE (5) or Quits
     * (6) and selects Y when prompted to Save. Unsaved changes, though stored
     * in memory, are wiped otherwise.
     */
    private void editOrder() {
        ArrayList<Order> orders = store.getOrders();
        String newDate = "";
        if (orders.isEmpty()) {
            con.print("\nStore records are empty. Create a new order from the main menu.");
            con.readString("\nPress ENTER to return to main menu...");
        } else {
            displayDates();

            String userDate = con.readDate();

            ArrayList<String> dates = store.getDates();
            int min = Integer.MAX_VALUE;
            int max = 0;
            if (dates.contains(userDate)) {
                for (Order o : orders) {
                    if (o.getFileNumber().equals(userDate)) {
                        Tax t = o.getTaxInfo();
                        Product p = o.getType();
                        con.print("\nOrder Number:\t\t" + o.getOrderNumber()
                                + "\nCustomer Name:\t\t" + o.getCustomerName()
                                + "\nProduct name:\t\t" + p.getProductName()
                                + "\nState:\t\t\t" + t.getState()
                                + "\nArea:\t\t\t" + df.format(o.getArea())
                                + "\n--------------------------------"
                                + "\nTOTAL:\t\t\t$" + df.format(o.getTotal()) + "\n--------------------------------\n");
                        if (o.getOrderNumber() > max) {
                            max = o.getOrderNumber();
                        }
                        if (o.getOrderNumber() < min) {
                            min = o.getOrderNumber();
                        }
                    }
                }

                Order temp = new Order();
                boolean changeExists = false;
                int orderNumber = con.readInt("\nWhich order would you like to edit?", min, max);
                for (Order o : orders) {
                    if (o.getOrderNumber() == orderNumber) {
                        newDate = con.readEditedDate(o.getFileNumber());
                        o.setFileNumber(newDate);
                        Tax t = o.getTaxInfo();
                        Product p = o.getType();
                        String name;
                        con.print("\nEnter customer name (" + o.getCustomerName() + "): [Press ENTER to skip]");
                        String tempName = con.readString("");
                        if (tempName.equals("")) {
                            name = o.getCustomerName();
                        } else {
                            name = tempName;
                        }

                        Double area;
                        con.print("\nEnter area (" + o.getArea() + "): [Press ENTER to skip]");
                        String tempArea = con.readString("");
                        if (tempArea.equals("")) {
                            area = o.getArea();
                        } else {
                            area = Double.parseDouble(tempArea);
                        }

                        String state = t.getState();
                        o.setTaxInfo(checkStateExists(state, true));

                        String productName = p.getProductName();
                        o.setType(checkProductExists(productName, true));

                        o.setCustomerName(name);
                        o.setArea(area);

                        o.setMaterialCost(o.getArea() * p.getCostPerSqFt());
                        o.setLaborCost(o.getArea() * p.getLaborCostPerSqFt());
                        o.setTax((o.getMaterialCost() + o.getLaborCost()) * t.getTaxRate() / 100);
                        o.setTotal(o.getMaterialCost() + o.getLaborCost() + o.getTax());

                        temp = o;
                        changeExists = true;

                    }
                }

                con.print("\n");
                con.print("EDITED ORDER RECEIPT - YOUR ORDER IS NOT YET COMPLETE\n");
                con.print("Date: \t\t" + userDate.substring(0, 2) + "-" + userDate.substring(2, 4) + "-" + userDate.substring(4, 8) + "\n");
                con.print("Customer Name: \t" + temp.getCustomerName() + "\n");
                con.print("Area: \t\t" + df.format(temp.getArea()) + "sq.ft.\n");
                con.print("Material Cost: \t" + "$" + df.format(temp.getMaterialCost()) + "\n");
                con.print("Labor Cost: \t" + "$" + df.format(temp.getLaborCost()) + "\n");
                con.print("Tax: \t\t" + "$" + df.format(temp.getTax()) + "\n");
                con.print("TOTAL: \t\t" + "$" + df.format(temp.getTotal()) + "\n");
                String userSure = con.readString("Order number " + orderNumber + " is about to be edited. Is this information correct? Y/N");

                if (changeExists) {
                    if (userSure.toLowerCase().equals("y")) {
                        store.updateEditedOrder(userDate, orderNumber, temp);
                        store.updateEditedDates(newDate);
                        con.print("\nOrder was successfully changed.");
                        con.readString("\nPress ENTER to return to main menu...");
                    } else {
                        con.print("No changes were made to your order.");
                        con.readString("\nPress ENTER to return to main menu...");
                    }
                } else {
                    con.print("\nOrder could not be changed.");
                    con.readString("\nPress ENTER to return to main menu...");
                }
            } else {
                con.print("\nNo orders were found for this date.");
                con.readString("\nPress ENTER to return to main menu...");
            }
        }

    }

    private void removeOrder() {
        ArrayList<Order> orders = store.getOrders();
        if (orders.isEmpty()) {
            con.print("\nStore records are empty. Create a new order from the main menu.");
            con.readString("\nPress ENTER to return to main menu...");
        } else {
            displayDates();

            String userDate = con.readDate();

            ArrayList<String> dates = store.getDates();
            int min = Integer.MAX_VALUE;
            int max = 0;
            if (dates.contains(userDate)) {
                for (Order o : orders) {
                    if (o.getFileNumber().equals(userDate)) {
                        Tax t = o.getTaxInfo();
                        Product p = o.getType();
                        con.print("\nOrder Number:\t\t" + o.getOrderNumber()
                                + "\nCustomer Name:\t\t" + o.getCustomerName()
                                + "\nProduct name:\t\t" + p.getProductName()
                                + "\nState:\t\t\t" + t.getState()
                                + "\nArea:\t\t\t" + df.format(o.getArea())
                                + "\n--------------------------------"
                                + "\nTOTAL:\t\t\t$" + df.format(o.getTotal()) + "\n--------------------------------\n");
                        if (o.getOrderNumber() > max) {
                            max = o.getOrderNumber();
                        }
                        if (o.getOrderNumber() < min) {
                            min = o.getOrderNumber();
                        }
                    }
                }

                Order temp = new Order();
                boolean orderDeleted = false;
                int orderNumber = con.readInt("\nWhich order would you like to delete?", min, max);
                for (Order o : orders) {
                    if (o.getOrderNumber() == orderNumber) {
                        temp = o;
                        orderDeleted = true;
                    }

                }
                if (orderDeleted) {
                    String userSure = con.readString("Order number " + orderNumber + " is about to be deleted. Are you sure? Y/N");
                    if (userSure.toLowerCase().equals("y")) {
                        store.updateDeletedOrder(userDate, orderNumber, temp);
                        store.updateDeletedDates(userDate, temp);
                        con.print("\nOrder was successfully deleted.");
                        con.readString("\nPress ENTER to return to main menu...");
                    } else {
                        con.print("\nOrder was not deleted.");
                        con.readString("\nPress ENTER to return to main menu...");
                    }
                } else {
                    con.print("\nOrder could not be deleted.");
                    con.readString("\nPress ENTER to return to main menu...");
                }
            } else {
                con.print("\nNo orders were found for this date.");
                con.readString("\nPress ENTER to return to main menu...");
            }
        }
    }

    private void saveOrder() throws IOException {
        if (testMode == false) {
            store.writeDates();
            store.writeOrders();
            store.deleteEmptyFiles();
            con.print("\nOrders saved.");
            con.readString("\nPress ENTER to continue...");
        } else {
            taxdao.writeTaxes();
            prodao.writeProducts();
            con.print("\nAny updated State or Product information has been saved."
                    + "\nCannot save orders to file in test mode.");
            con.readString("\nPress ENTER to continue...");
        }
    }

    private void displayStateNames() {
        ArrayList<Tax> tempTax = taxdao.getTaxes();

        con.print("\nOur stores are located in the following locations:\n");
        for (Tax t : tempTax) {
            con.print("+ " + t.getState() + "\t");
        }
        con.print("\n");
    }

    private Tax checkStateExists(String state, boolean editState) {
        ArrayList<Tax> tempTax = taxdao.getTaxes();
        Tax tempTaxClass = new Tax();
        boolean stateExists = false;
        String currentState = state;
        while (stateExists == false) {

            if (editState) {
                con.print("\nEnter state (" + currentState + "): [Press ENTER to skip]");
                String tempState = con.readString("");
                if (tempState.equals("")) {
                    state = currentState;
                } else {
                    state = tempState;
                }
            } else {
                state = con.readString("\nEnter state:");
            }
            for (Tax t : tempTax) {

                if (t.getState().toLowerCase().equals(state.toLowerCase())) {
                    tempTaxClass.setState(t.getState());
                    tempTaxClass.setTaxRate(t.getTaxRate());
                    stateExists = true;
                }

            }

            if (stateExists == false) {
                con.print("\nWe don't work there. Try a different state.");
            }

        }

        return tempTaxClass;

    }

    private void displayProductNames() {
        ArrayList<Product> tempProduct = prodao.getProducts();

        con.print("\nWe sell the following products:\n");
        for (Product p : tempProduct) {
            con.print("+ " + p.getProductName() + "\t");
        }
        con.print("\n");
    }

    private Product checkProductExists(String productName, boolean editProduct) {
        ArrayList<Product> tempProduct = prodao.getProducts();
        Product tempProductClass = new Product();
        boolean productExists = false;
        String currentProductName = productName;
        while (productExists == false) {

            if (editProduct) {
                con.print("\nEnter product name (" + productName + "): [Press ENTER to skip]");
                String tempProductName = con.readString("");
                if (tempProductName.equals("")) {
                    productName = currentProductName;
                } else {
                    productName = tempProductName;
                }
            } else {
                productName = con.readString("\nEnter product name:");
            }
            for (Product p : tempProduct) {
                if (p.getProductName().toLowerCase().equals(productName.toLowerCase())) {

                    tempProductClass.setProductName(p.getProductName());
                    tempProductClass.setCostPerSqFt(p.getCostPerSqFt());
                    tempProductClass.setLaborCostPerSqFt(p.getLaborCostPerSqFt());
                    productExists = true;

                }

            }

            if (productExists == false) {
                con.print("\nWe don't sell that. Try a different store, or try again.");
            }

        }

        return tempProductClass;
    }

    /**
     * *
     *
     * Retrieves new
     *
     * @param userDate
     * @return
     */
    private Order getNewOrderData(String userDate) {
        String customerName = con.readString("\nEnter customer name.");
        String state = "";
        String productName = "";
        double costPerSqFt = 0.0;
        double laborCostPerSqFt = 0.0;
        double materialCost = 0.0;
        double laborCost = 0.0;
        double taxRate = 0.0;
        double tax = 0.0;
        double total = 0.0;
        displayStateNames();
        Tax tempTax = checkStateExists(state, false);
        displayProductNames();
        Product tempProd = checkProductExists(productName, false);
        double area = con.readDouble("\nEnter the required area for " + tempProd.getProductName(), 0, Double.MAX_VALUE);

        materialCost = area * tempProd.getCostPerSqFt();
        laborCost = area * tempProd.getLaborCostPerSqFt();
        tax = (materialCost + laborCost) * tempTax.getTaxRate() / 100;
        total = materialCost + laborCost + tax;

        Order temp = new Order();
        temp.setCustomerName(customerName);
        temp.setArea(area);
        temp.setMaterialCost(materialCost);
        temp.setLaborCost(laborCost);
        temp.setTax(tax);
        temp.setTotal(total);
        temp.setTaxInfo(tempTax);
        temp.setType(tempProd);

        int max = 0;
        ArrayList<Order> orders = store.getOrders();
        for (Order o : orders) {
            //if (o.getFileNumber().equals(userDate)) {
            if (o.getOrderNumber() > max) {
                max = o.getOrderNumber();
            }
            //}
        }

        temp.setOrderNumber(max + 1);
        temp.setFileNumber(userDate);
        return temp;
    }

    //ADMIN or TEST mode-only methods
    /**
     * *
     * createProduct() creates a new Product object while in Admin mode.
     */
    public void createProduct() {
        Product newProduct = new Product();
        newProduct.setProductName(con.readString("\nEnter the new product name:"));
        newProduct.setCostPerSqFt(con.readDouble("\nEnter the cost per sq. ft.:", 0, Double.MAX_VALUE));
        newProduct.setLaborCostPerSqFt(con.readDouble("\nEnter the labor cost per sq. ft.", 0, Double.MAX_VALUE));

        prodao.addNewProduct(newProduct);
        con.print("\nNew product added.");
        con.readString("\nPress ENTER to return to main menu...");
    }

    /**
     * *
     * editProduct() edits an existing product. Only available in Admin/Test
     * mode.
     */
    public void editProduct() {
        ArrayList<Product> products = prodao.getProducts();
        displayProductNames();
        String productToEdit = con.readString("\nEnter the product name that you want to edit:");
        //String currentProductName = "";
        Product p = checkProductExists(productToEdit, true);
        Double currentCostPerSqFt = p.getCostPerSqFt();
        Double currentLaborCostPerSqFt = p.getLaborCostPerSqFt();

        con.print("\nEnter cost per sq. ft. (" + currentCostPerSqFt + "): [Press ENTER to skip]");
        String tempCostPerSqFt = con.readString("");
        if (tempCostPerSqFt.equals("")) {
            p.setCostPerSqFt(currentCostPerSqFt);
        } else {
            p.setCostPerSqFt(Double.parseDouble(tempCostPerSqFt));
        }

        con.print("\nEnter labor cost per sq. ft. (" + currentLaborCostPerSqFt + "): [Press ENTER to skip]");
        String tempLaborCostPerSqFt = con.readString("");
        if (tempLaborCostPerSqFt.equals("")) {
            p.setLaborCostPerSqFt(currentLaborCostPerSqFt);
        } else {
            p.setLaborCostPerSqFt(Double.parseDouble(tempLaborCostPerSqFt));
        }

        prodao.updateProducts(p);
        con.print("\nProduct Information Successfully Changed.");
        con.readString("\nPress ENTER to return to main menu...");
    }

    /**
     * *
     * Removes a Product from ArrayList; only available in Admin mode.
     */
    public void deleteProduct() {
        ArrayList<Product> products = prodao.getProducts();
        displayProductNames();
        //String productToDelete = con.readString("\nEnter the product you want to delete:");
        String productToDelete = "";
        Product p = checkProductExists(productToDelete, false);

        String userSure = con.readString("You are about to delete " + p.getProductName() + " from the inventory.\nDo you want to continue? (Y/N)");
        if (userSure.toLowerCase().equals("y")) {
            prodao.removeProduct(p);
            con.print("\nProduct successfully deleted.");
            con.readString("\nPress ENTER to return to main menu...");
        } else {
            con.print("\nProduct was not deleted.");
            con.readString("\nPress ENTER to return to main menu...");
        }

    }

    /**
     * *
     * Creates a new State object; can only be used in Admin mode.
     */
    public void createState() {
        Tax newTax = new Tax();
        newTax.setState(con.readString("\nEnter the state:"));
        newTax.setTaxRate(con.readDouble("\nEnter the tax rate:", 0.1, 100));

        taxdao.addNewState(newTax);
        con.print("\nNew state tax information added");
        con.readString("\nPress ENTER to return to main menu...");

    }

    /**
     * *
     * Edits a State object; only for use in Admin mode.
     */
    public void editState() {
        ArrayList<Tax> taxes = taxdao.getTaxes();
        displayStateNames();
        String stateToEdit = con.readString("\nEnter the state name that you want to edit:");
        String currentState = "";
        Tax t = checkStateExists(stateToEdit, true);
        Double currentTaxRate = t.getTaxRate();

        con.print("\nEnter tax rate (" + currentTaxRate + "): [Press ENTER to skip]");
        String tempTaxRate = con.readString("");
        if (tempTaxRate.equals("")) {
            t.setTaxRate(currentTaxRate);
        } else {
            t.setTaxRate(Double.parseDouble(tempTaxRate));
        }

        taxdao.updateTaxes(t);
        con.print("\nState Tax Information Successfully Changed.");
        con.readString("\nPress ENTER to return to main menu...");

    }

    /**
     * *
     * Removes a State from use in Prod mode; can only be called in Admin mode.
     */
    public void deleteState() {
        ArrayList<Tax> taxes = taxdao.getTaxes();
        displayStateNames();
        //String stateToDelete = con.readString("\nEnter the state you want to delete:");
        String stateToDelete = "";
        Tax t = checkStateExists(stateToDelete, false);

        String userSure = con.readString("You are about to delete " + t.getState() + " from the inventory.\nDo you want to continue? (Y/N)");
        if (userSure.toLowerCase().equals("y")) {
            taxdao.removeState(t);
            con.print("\nState tax information was successfully deleted.");
            con.readString("\nPress ENTER to return to main menu...");
        } else {
            con.print("\nState tax information was not deleted.");
            con.readString("\nPress ENTER to return to main menu...");
        }

    }

    /**
     * *
     * Displays the current List of Dates for which orders are present. Denotes
     * the first two characters as Month, the next two as Day, and the remaining
     * four as the Year. MMDDYYYY. Used throughout methods in the Controller.
     */
    public void displayDates() {
        ArrayList<String> dates = store.getDates();
        con.print("\nOrder information is available for the following dates:");
        for (String d : dates) {
            con.print("\n" + "//\t" + d.substring(0, 2) + "-" + d.substring(2, 4) + "-" + d.substring(4, 8));
        }
    }

    /**
     * *
     * Loads the current ConfigFile.txt, in which "true" denotes Test/Admin
     * mode; can be manually toggled off by editing the file. A "false"
     * designation allows Production mode. Order files cannot be altered in Test
     * mode, while CRUD methods for States/Taxes and Products may only be
     * modified while in Test mode.
     *
     * @throws FileNotFoundException
     */
    private void loadConfig() throws FileNotFoundException {
        Scanner loader = new Scanner(new BufferedReader(new FileReader("configFile.txt")));

        while (loader.hasNext()) {
            String currentLine = loader.next();

            if ("true".equals(currentLine)) {
                testMode = true; //test or admin mode
            } else if ("false".equals(currentLine)) {
                testMode = false; // production mode
            }

        }

    }

}
