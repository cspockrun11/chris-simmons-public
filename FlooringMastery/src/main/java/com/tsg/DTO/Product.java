/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.DTO;

import java.util.Objects;

/**
 *
 * @author apprentice
 */
public class Product {
    
    private String productName;
    private double costPerSqFt;
    private double laborCostPerSqFt;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getCostPerSqFt() {
        return costPerSqFt;
    }

    public void setCostPerSqFt(double costPerSqFt) {
        this.costPerSqFt = costPerSqFt;
    }

    public double getLaborCostPerSqFt() {
        return laborCostPerSqFt;
    }

    public void setLaborCostPerSqFt(double laborCostPerSqFt) {
        this.laborCostPerSqFt = laborCostPerSqFt;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 31 * hash + Objects.hashCode(this.productName);
        hash = 31 * hash + (int) (Double.doubleToLongBits(this.costPerSqFt) ^ (Double.doubleToLongBits(this.costPerSqFt) >>> 32));
        hash = 31 * hash + (int) (Double.doubleToLongBits(this.laborCostPerSqFt) ^ (Double.doubleToLongBits(this.laborCostPerSqFt) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Product other = (Product) obj;
        if (Double.doubleToLongBits(this.costPerSqFt) != Double.doubleToLongBits(other.costPerSqFt)) {
            return false;
        }
        if (Double.doubleToLongBits(this.laborCostPerSqFt) != Double.doubleToLongBits(other.laborCostPerSqFt)) {
            return false;
        }
        if (!Objects.equals(this.productName, other.productName)) {
            return false;
        }
        return true;
    }
    
    
    
}
